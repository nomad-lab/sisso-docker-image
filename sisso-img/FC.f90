program feature_construction

!----------------------------------------------------------------------------------
!Copyright 2017 Runhai Ouyang
!
!   Licensed under the Apache License, Version 2.0 (the "License");
!   you may not use this file except in compliance with the License.
!   You may obtain a copy of the License at
!
!       http://www.apache.org/licenses/LICENSE-2.0
!
!   Unless required by applicable law or agreed to in writing, software
!   distributed under the License is distributed on an "AS IS" BASIS,
!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
!   See the License for the specific language governing permissions and
!   limitations under the License.
!----------------------------------------------------------------------------------

! ********************************************************************************************************************
! Feature construction by mathematical combinations between input variables (primary features)
! Sure independent screening will be used for 'optimal' subspace selection
! Version FCDI.1.0, April, 2017
! *********************************************************************************************************************

use libms  ! library
implicit none
include 'mpif.h'  ! parallel 

integer lname,maxop,maxcomb,iFCDI,stat,stat2,stat3
real*8 tmp,aaa,bbb,PI,tmp_score(2),maxfval_lb,maxfval_ub,width
parameter (lname=150,maxop=20,maxcomb=10,PI=3.14159265358979d0)
integer*8 i,j,k,l,ll,mm1,mm2,nselect,nf(maxcomb),ndimtype,ntot,nthis,vfsize,nvf,nvf_old,subs_sis,niter_sis,nout_sis,nnomb
integer nsf,nif,icomb,rung,ntask_nc,ntask_1c,nele,nreject,loc(1),nnn,nvf_init,ntask_tot
character line*1000000,opset(maxop)*200,phiname*5,line_para*500,ptype*10,dimclass*500,vf2sf*10,desc_file*100,&
          nsample_line*500,task_arr*2
logical foutsave,sis_s_on,lreject,ly0,allele,lFCDI
real*8,allocatable:: trainy(:,:),y0(:,:),fin(:,:),fout(:,:),fselected(:,:),sfeat(:,:),dimin(:,:),dimout(:,:),&
ftag(:),SD(:,:),sisfeat(:,:),tag(:),vfeat(:,:,:),ftag_select(:),score_select(:,:),trainy_c(:,:)
integer*8,allocatable:: neach(:),nsample(:),order(:),nvfeach(:),ngroup(:,:)
character(len=lname),allocatable:: cname(:)*100,name_fin(:),lastop_in(:)*10,name_fout(:),lastop_out(:)*10,name_fselected(:),&
sfname(:),name_sisfeat(:),vfname(:)*50,reject(:)
logical,allocatable:: available(:)

! MPI variables
real*8 stime,etime,time_start,time_end
integer*8 mpii,mpij,mpik
integer mpierr,mpirank,mpisize,status(MPI_STATUS_SIZE)
integer*8,allocatable:: mpin(:)
logical mpion
common mpierr,mpirank,mpisize
!------------------------------------------------------------

!------------------------------
! MPI starts
!------------------------------
call mpi_init(mpierr)
call mpi_comm_size(mpi_comm_world,mpisize,mpierr)
call mpi_comm_rank(mpi_comm_world,mpirank,mpierr)
if(mpirank==0) stime=mpi_wtime()
!------------------------------------------------


!-----------------------
!initialization
!-----------------------
sis_s_on=.false.
nvf_init=0

nsf= 0                    ! number of scalar features
task_arr='1c'             ! how tasks are arranged in the train.dat? '1c': in one columns; 'nc': in many columns
ntask_tot=1               ! total tasks
ntask_nc= 1               ! number of tasks arranged in n columns
ntask_1c= 1               ! number of tasks arranged in one column
!nsample= 1,1             ! number of samples for each task for quantitative properties
nvf= 0                    ! number of vector features
vfsize= 0                 ! size of vector (all vectors have the same size)
vf2sf= 'sum'              ! transform vector to scalar features: sum,norm,min,max

ndimtype=2                ! number of dimension types
dimclass='(1:2)(3:4)'     ! specify features for each type (not for dimensionless features)
allele=.false.            ! Should all elements appear in each of the selected features?
nele=1                    ! number of elements(<=6), ONLY useful when allele=.true.
maxfval_lb=1d-8           ! features having the max. abs. data value <maxfval_lb will not be selected
maxfval_ub=1d5            ! features having the max. abs. data value >maxfval_ub will not be selected

rung=1                    ! The rung (complexity) of the feature space to be constructed
opset=''                  ! available operators:(+)(-)(*)(/)(exp)(exp-)(^-1)(^2)(^3)(sqrt)(cbrt)(log)(|-|)(SCD)(^6)
                          ! (sin)(cos)

subs_sis=1                ! size of each subspace to be selected by SIS

ptype='quanti'            ! property type: 'quanti'(quantitative),'quali'(qualitative)
width=0.d0                ! used for 'quali', the classification tolerance at the domain boundaries.

!------------------------------------
!parameters readin from FCDI.in
!------------------------------------
open(100,file='FCDI.in',status='old')
do
   read(100,'(a)',iostat=stat), line_para
   if (stat /= 0) exit
   if(index(line_para,'!')/=0) line_para(index(line_para,'!'):)=''
   i=index(line_para,'=')
   if(i>0) then
   select case (trim(adjustl(line_para(1:i-1))))
!--
   case('nsf')
   read(line_para(i+1:),*) nsf
   case('ntask')
   read(line_para(i+1:),*) ntask_tot
   case('task_arr')
   read(line_para(i+1:),*) task_arr
   case('nsample') ! number of samples  for each task
   read(line_para(i+1:),'(a)') nsample_line
   case('nvf')
   read(line_para(i+1:),*) nvf
   nvf_init=nvf
   case('vfsize')
   read(line_para(i+1:),*) vfsize
   case('vf2sf')
   read(line_para(i+1:),*) vf2sf

   case('ndimtype')
   read(line_para(i+1:),*) ndimtype
   case('dimclass')
   read(line_para(i+1:),'(a)') dimclass
   case('maxfval_lb')
   read(line_para(i+1:),*) maxfval_lb
   case('maxfval_ub')
   read(line_para(i+1:),*) maxfval_ub
!--
   case('rung')
   read(line_para(i+1:),*) rung
   case('nele')
   read(line_para(i+1:),*) nele
   case('opset')
   read(line_para(i+1:),*) opset(:rung)
   case('allele')
   read(line_para(i+1:),*) allele
!--
   case('subs_sis')
   read(line_para(i+1:),*) subs_sis
   niter_sis=subs_sis      ! default value for niter_sis (iterative SIS)
   case('niter_sis')
   read(line_para(i+1:),*) niter_sis
   case('ptype')
   read(line_para(i+1:),*) ptype
   case('width')
   read(line_para(i+1:),*) width
!--
   end select
   end if
end do
close(100)


if(task_arr=='1c') then
 ntask_1c=ntask_tot
else if(task_arr=='nc') then
 ntask_nc=ntask_tot
end if


allocate(nsample(ntask_1c))  ! total samples for each task
allocate(ngroup(ntask_1c,1000))  ! (classification) number of samples for each of the groups in each task
nsample=0
ngroup=0
if(trim(adjustl(ptype))=='quanti') then
  read(nsample_line,*) nsample
else
  do ll=1,ntask_1c
    i=index(nsample_line,'(')
    j=index(nsample_line,')')
    l=0
    do k=i,j
       if(nsample_line(k:k)==',') l=l+1
    end do
    read(nsample_line(i+1:j-1),*) ngroup(ll,1:l+1)
    ngroup(ll,1000)=l+1
    nsample(ll)=sum(ngroup(ll,1:l+1))
    nsample_line(:j)=''
  end do
end if

!------------------------------------------------------
! array allocation
!------------------------------------------------------
i=1000000/mpisize  ! initial array size, will be ajusted automatically during run
j=subs_sis*5  ! for selected features
k=100000/mpisize  ! for vector features
allocate(trainy(sum(nsample),ntask_nc))
allocate(trainy_c(sum(nsample),ntask_nc))
allocate(y0(sum(nsample),ntask_nc))
allocate(fin(sum(nsample),i))
allocate(name_fin(i))
allocate(lastop_in(i))
allocate(dimin(ndimtype,i))
allocate(fout(sum(nsample),i))
allocate(name_fout(i))
allocate(lastop_out(i))
allocate(dimout(ndimtype,i))
allocate(sfeat(sum(nsample),nsf))
allocate(fselected(sum(nsample),j))
allocate(ftag_select(j))
allocate(score_select(j,2))
allocate(name_fselected(j))
allocate(sfname(nsf))
allocate(cname(sum(nsample)))
allocate(mpin(mpisize))
allocate(neach(mpisize))
allocate(SD(ntask_1c,ntask_nc))
allocate(vfeat(sum(nsample),vfsize,k))
allocate(nvfeach(mpisize))
if(nvf_init>0) allocate(vfname(nvf))
allocate(tag(sum(nsample))) ! make each feature unique by creating feature tags ftag=<feature,tag>
do j=1,ntask_1c
do i=1,nsample(j)    
  tag(sum(nsample(:j-1))+i)=1.0d0+0.001d0*i
end do
end do

! asign initial values to dimension
dimin=0.d0   ! dimensionless
do ll=1,ndimtype
  i=index(dimclass,'(')
  j=index(dimclass,':')
  if(i>0 .and. j>0) then
    read(dimclass(i+1:j-1),*) k
    i=index(dimclass,')')
    read(dimclass(j+1:i-1),*) l
    dimin(ll,k:l)=1.d0 
    dimclass(:i)=''
  end if
end do


!---------------------------------

! output parameter for confirmation
if(mpirank==0) then
open(99,file='FC.out',status='replace')
write(99,'(a)') 'Version FCDI.1.0, April, 2017'
write(99,'(/a)') 'Parameters read from FCDI.in:'
write(99,'(a)')  '---------------------------------------------------------------------------------'
write(99,'(a,i8)')  'number of scalar features: ',nsf
write(99,'(a,i8)')  'total number of tasks: ',ntask_tot
write(99,'(a,a)')  'arrangement of the tasks: ',task_arr
1001 format(a,*(i8))
write(99,1001)  'number of samples for each task: ',nsample
write(99,'(a,i8)')  'number of vector features: ',nvf
write(99,'(a,i8)')  'size of each vector feature: ',vfsize
write(99,'(a,a)')  'transformation of vector to scalar features: ',trim(vf2sf)
write(99,'(a,i8)') 'number of unit-type (for dimension analysis): ',ndimtype
write(99,'(a)') 'dimension for each primary feature '
1002 format(*(f6.2))
do i=1,nsf+nvf
write(99,1002) dimin(:,i)
end do
write(99,'(a,a)') 'property type: "quanti"(quantitative),"quali"(qualitative): ',trim(ptype)
if(trim(adjustl(ptype))=='quali') then
1003 format(a,i3,a,*(i5))
  do i=1,ntask_1c
     write(99,1003) 'Number of samples in each group of task ',i,': ',ngroup(i,:ngroup(i,1000))
  end do
write(99,'(a,f10.6)') 'Classification tolerance at the domain boundaries: ',width
end if


write(99,'(a,e20.10)') 'lower bound of the max abs. data value for the selected features: ',maxfval_lb
write(99,'(a,e20.10)') 'upper bound of the max abs. data value for the selected features: ',maxfval_ub
write(99,'(/a,i8)')  'number of iteration (the rung) for feature space construction: ',rung
1004 format(*(a))
write(99,1004)  'operator set for each iteration of feature combination: ',(trim(opset(j)),'; ',j=1,rung)
write(99,'(a,l6)')  'Should all elements appear in each of the selected features ? ',allele
write(99,'(a,i8)')  'number of elements in each sample: ',nele

write(99,'(a,i8)') 'total number of features after screening (the subspace size): ',subs_sis
!write(99,'(a,i8)') 'number of selected features in each inner iterative screening cycle: ',niter_sis

write(99,'(a/)')  '---------------------------------------------------------------------------------'
end if
!---------------------------------------------------------------------------------------------------------


! avoid repetition of features from existed space
inquire(file='reject.name',exist=lreject)
if(lreject) then
   nreject=0
   open(100,file='reject.name',status='old')
   ! count total number
   do 
   read(100,*,iostat=stat2)
   if (stat2 /= 0) exit
   nreject=nreject+1
   end do
   allocate(reject(nreject))
   rewind(100)
   ! read in
   do j=1,nreject
   read(100,'(a)') line
   call string_split(line,reject(j:j),' ')
   if(reject(j)(1:1)==' ') reject(j)=adjustl(reject(j))
   end do
   close(100)
   !ordering
   do i=1,nreject-1
       loc(1)=get_max(reject(i:nreject))
       if(loc(1)/=1) then
       line=trim(reject(i))
       reject(i)=reject(i+loc(1)-1)
       reject(i+loc(1)-1)=trim(line)
       end if
   end do
end if


!-------------------
! data read in
!-------------------

! train.dat
open(1,file='train.dat',status='old')
  read(1,'(a)') line
  call string_split(line,name_fin(1:nsf+ntask_nc+1),' ')
  if(trim(adjustl(ptype))=='quanti') then
     sfname=name_fin(ntask_nc+2:nsf+ntask_nc+1)
  else
     sfname=name_fin(2:nsf+1)
  end if

  do i=1,sum(nsample)
      read(1,'(a)') line
      call string_split(line,name_fin(1:nsf+ntask_nc+1),' ')
      cname(i)=trim(adjustl(name_fin(1)))
      j=index(line,trim(adjustl(cname(i))))
      k=index(line(j:),' ')
    
      if(trim(adjustl(ptype))=='quanti') then
        read(line(j+k-1:),*) trainy(i,:),sfeat(i,:)
      else
        read(line(j+k-1:),*) sfeat(i,:)   ! no y value in the train.dat file for qualitative property
        trainy(i,:)=0.d0    ! 0 denote unclassified
      end if
  end do

close(1)
fin(:,1:nsf)=sfeat
name_fin(:nsf)=sfname


iFCDI=0
inquire(file='iFCDI',exist=lFCDI)
if(lFCDI) then
  open(1,file='iFCDI',status='old')
  read(1,*) iFCDI
  close(1)
end if

! read residual data for classification
if(trim(adjustl(ptype))=='quali' .and. iFCDI==2) then
   k=0
   do i=1,ntask_1c
     write(desc_file,'(a,i3.3,a)') '../iter01/desc_dat/desc1_',i,'.dat'
     open(1,file=trim(adjustl(desc_file)),status='old')
     read(1,*)
     do j=1,nsample(i)
        k=k+1
        read(1,*) tmp,trainy(k,1)    ! 1: classified, 0: unclassified
     end do
     close(1)
   end do      
end if

! train_vf.dat
if(nvf_init>0) then
  open(1,file='train_vf.dat',status='old')
  read(1,'(a)') line   ! title line
  call string_split(line,vfname,' ')  ! there are $nvf 'vfname'
  do i=1,sum(nsample)
     read(1,*) (vfeat(i,:,j),j=1,nvf)
  end do
  close(1)
  do i=1,nvf
  fin(1,nsf+i)=i    ! assign vf ID to the 1st element of fin
  fin(2,nsf+i)=abs(sum(tag*sum(vfeat(:,:,i),2)))/ntask_1c ! tag each features and put to fin
  name_fin(nsf+i)=vfname(i)  ! identify the vf by their name v_xxx
  end do
end if

!number of integer features, see function isreal()
!nif=0
!do i=1,nsf+nvf
!if(index(name_fin(i),'si_')/=0 .or. index(name_fin(i),'vi_')/=0) nif=nif+1
!end do

! max size of selected space to initiate sis_s, see function goodf()
!max size=[2*subs_sis,min[(5G memory)/(sample_size*double_precision*mpisize),50*subs_sis] ]
nnn= max(2*subs_sis,min(int8(500000000/(sum(nsample)*mpisize)),50*subs_sis))


!---------------------------------------

!----------
! SD
!----------
do i=1,ntask_nc
do j=1,ntask_1c
mm1=sum(nsample(:j-1))+1
mm2=sum(nsample(:j))
trainy_c(mm1:mm2,i)=trainy(mm1:mm2,i)-sum(trainy(mm1:mm2,i))/(mm2-mm1+1)
SD(j,i)=sqrt(sum((trainy_c(mm1:mm2,i))**2)/(mm2-mm1+1))  ! standard deviation
end do
1005 format(a,i3.3,a,*(f10.5))
if(mpirank==0 .and. trim(adjustl(ptype))=='quanti') &
        write(99,1005) 'Standard Deviation (SD) of property (',i,'): ',SD(:,i)
end do

!--------------------------------------------------


!---------------------------------------------------
! feature combinations start ...
! see combine function for the function details
! phi0 is the initial primary feature space
!-------------------------------------------------------------------------

!------------------------
!phi0
!------------------------
nselect=0 ! for all subspace
nf=0      ! for this subspace only
foutsave=.true.
mpion=.false.
nvf_old=nvf  ! back up
lastop_in='' ! the last applied operator of a feature
lastop_out=''

i=nsf+nvf ! total number of feature
call combine(fin(:,1:i),name_fin(1:i),lastop_in(1:i),dimin(:,1:i),fin(:,1:i),name_fin(1:i),lastop_in(1:i),&
dimin(:,1:i),foutsave,j,'NO')

nvf=nvf_old  ! no combination
ntot=nsf+nvf
nthis=ntot

if(mpirank==0) write(*,'(/)')
if(mpirank==0) call writeout('phi00',nselect,nvf,ntot)
!-----------------------------------------------------------------------

call mpi_barrier(mpi_comm_world,mpierr)

!--------------------
!starting from phi1
!--------------------
if(mpirank/=0) nselect=0
mpion=.true.

do icomb=1,rung

    !allocating jobs for each core
    !1. CXi+Xi(Xi-1)/2+Xi(N-sum(Xi->Xj))=CXn+Xn(Xn-1)/2
    !2. (CXn+Xn(Xn-1)/2)*n=CN+N(N-1)/2
    !with 1 and 2 we get Xi

    mpin=0
    do mpii=1,mpisize
       aaa=(2*sum(mpin(:mpii-1))+1-2*ntot)
       bbb=(nthis**2-nthis+2*nthis*(ntot-nthis))/float(mpisize)
       mpin(mpii)=min(int8(nint((-aaa-sqrt(aaa**2-4*bbb))/2.0)),nthis-sum(mpin(:mpii-1)))
    end do
    mpin(mpisize)=nthis-sum(mpin(:mpisize-1))

    if(icomb==rung) foutsave=.false.

    nvf_old=nvf
    ! fin1: nthis; fin2: total
    call combine(fin(:,ntot-nthis+sum(mpin(:mpirank))+1:ntot-nthis+sum(mpin(:mpirank+1))),&
               name_fin(ntot-nthis+sum(mpin(:mpirank))+1:ntot-nthis+sum(mpin(:mpirank+1))),&
              lastop_in(ntot-nthis+sum(mpin(:mpirank))+1:ntot-nthis+sum(mpin(:mpirank+1))),&
               dimin(:,ntot-nthis+sum(mpin(:mpirank))+1:ntot-nthis+sum(mpin(:mpirank+1))),&
               fin(:,:ntot),name_fin(:ntot),lastop_in(:ntot),dimin(:,:ntot),foutsave,nf(icomb),&
               trim(adjustl(opset(icomb))))

  IF (foutsave) THEN

       !---------------------------
       ! redundant check
       !---------------------------
       call mpi_barrier(mpi_comm_world,mpierr)

       ! create neach: the size of this subspace generated on each core
       if(mpirank/=0) then
          call mpi_send(nf(icomb),1,mpi_integer8,0,1,mpi_comm_world,status,mpierr)
       else
          neach(1)=nf(icomb)
          do mpii=1,mpisize-1
               call mpi_recv(neach(mpii+1),1,mpi_integer8,mpii,1,mpi_comm_world,status,mpierr)
          end do
       end if
       call mpi_bcast(neach,mpisize,mpi_integer8,0,mpi_comm_world,mpierr)

       ! create ftag for redundant check
       allocate(ftag(nf(icomb)))
       do i=1,nf(icomb)
         if( isscalar(name_fout(i)) ) then
          ftag(i)=abs(sum(tag*fout(:,i)))/ntask_1c 
         else
          ftag(i)=fout(2,i)/ntask_1c
         end if
       end do

       ! allocate available array
       allocate(available(nf(icomb)))
        available=.true.
       ! create order : the last one store the size
       allocate(order(nf(icomb)+1))
 
       ! redundant check on each core: output order and available.
       if(mpirank==0) write(*,'(a,i15)') 'Number of newly generated features: ',sum(neach) 
       if(mpirank==0) write(*,'(a)') 'Redundant check on the newly generated features ...'
       call dup_scheck(neach(mpirank+1),ftag,name_fout,order,available)

       ! redundant check between cores,output available
       if(mpisize>1) call dup_pcheck(neach,ftag,name_fout,order,available)

       ! renew nvf and nvfeach on each core
       if(mpirank==0) then
          nvf=nvf_old
       else
          nvf=0
       end if
       do i=1,nf(icomb)
          if(available(i)) then
             if( .not. isscalar(name_fout(i)) ) nvf=nvf+1
          end if
       end do

       if(mpirank/=0) then
          call mpi_send(nvf,1,mpi_integer8,0,1,mpi_comm_world,status,mpierr)
       else
          nvfeach(1)=nvf
          do mpii=1,mpisize-1
               call mpi_recv(nvfeach(mpii+1),1,mpi_integer8,mpii,1,mpi_comm_world,status,mpierr)
          end do
       end if
       call mpi_bcast(nvfeach,mpisize,mpi_integer8,0,mpi_comm_world,mpierr)

       ! remove redundant features
       j=0
       if(mpirank==0) then
          nvf=nvf_old
       else
          nvf=0
       end if
       do i=1,nf(icomb)
          if(available(i)) then
             j=j+1
             fout(:,j)=fout(:,i)
             name_fout(j)=name_fout(i)
             lastop_out(j)=lastop_out(i)
             dimout(:,j)=dimout(:,i)
             ftag(j)=ftag(i)
             if( .not. isscalar(name_fout(j)) ) then
               nvf=nvf+1
               vfeat(:,:,nvf)=vfeat(:,:,nint(fout(1,j)))
               if(mpirank==0) fout(1,j)=nvf
               if(mpirank/=0) fout(1,j)=nvf+sum(nvfeach(:mpirank))
             end if
          end if
       end do
       nf(icomb)=j

       !renew neach
       if(mpirank/=0) then
          call mpi_send(nf(icomb),1,mpi_integer8,0,1,mpi_comm_world,status,mpierr)
       else
          neach(1)=nf(icomb)
          do mpii=1,mpisize-1
               call mpi_recv(neach(mpii+1),1,mpi_integer8,mpii,1,mpi_comm_world,status,mpierr)
          end do
       end if
       call mpi_bcast(neach,mpisize,mpi_integer8,0,mpi_comm_world,mpierr)
       available=.true.

      ! copy result from fout to fin
       nthis=sum(neach)
       if(mpirank==0) write(*,'(a,i15)') 'Number of newly generated features after redundant check: ',nthis
       ntot=ntot+nthis
       !--------
       if(mpirank/=0) then
         call mpi_send(fout(:,:nf(icomb)),sum(nsample)*nf(icomb),mpi_double_precision,0,2,&
                       mpi_comm_world,status,mpierr)
          call mpi_send(name_fout(:nf(icomb)),nf(icomb)*lname,mpi_character,0,3,mpi_comm_world,status,mpierr)
          call mpi_send(lastop_out(:nf(icomb)),nf(icomb)*10,mpi_character,0,4,mpi_comm_world,status,mpierr)
          call mpi_send(dimout(:,:nf(icomb)),ndimtype*nf(icomb),mpi_double_precision,0,5,mpi_comm_world,status,mpierr)
          if(nvf>0) call mpi_send(vfeat(:,:,:nvf),sum(nsample)*vfsize*nvf,&
                   mpi_double_precision,0,6,mpi_comm_world,status,mpierr)
       else
         if(ntot>ubound(fin,2)) call addm_in(ntot-ubound(fin,2))
         fin(:,ntot-nthis+1:ntot-nthis+neach(1))=fout(:,:neach(1))
         lastop_in(ntot-nthis+1:ntot-nthis+neach(1))=lastop_out(:neach(1))
         name_fin(ntot-nthis+1:ntot-nthis+neach(1))=name_fout(:neach(1))
         dimin(:,ntot-nthis+1:ntot-nthis+neach(1))=dimout(:,:neach(1))
         if(sum(nvfeach)>ubound(vfeat,3)) call addm_vf(sum(nvfeach)-ubound(vfeat,3)) ! vector feature
         do mpii=1,mpisize-1
              i=ntot-nthis+sum(neach(:mpii))+1
              j=ntot-nthis+sum(neach(:mpii+1))
              call mpi_recv(fin(:,i:j),sum(nsample)*neach(mpii+1),mpi_double_precision,mpii,2,&
                               mpi_comm_world,status,mpierr)
              call mpi_recv(name_fin(i:j),neach(mpii+1)*lname,mpi_character,mpii,3,&
                               mpi_comm_world,status,mpierr)
              call mpi_recv(lastop_in(i:j),neach(mpii+1)*10,mpi_character,mpii,4,mpi_comm_world,status,mpierr)
              call mpi_recv(dimin(:,i:j),ndimtype*neach(mpii+1),mpi_double_precision,&
                               mpii,5,mpi_comm_world,status,mpierr)
              if(nvfeach(mpii+1)>0) call mpi_recv(vfeat(:,:,nvf+1:nvf+nvfeach(mpii+1)),&
                sum(nsample)*vfsize*nvfeach(mpii+1),mpi_double_precision,mpii,6,mpi_comm_world,status,mpierr)
              nvf=nvf+nvfeach(mpii+1)
         end do
        end if

       nvf=sum(nvfeach)
       !---------------------------------------------
       ! collect total number of selected features
       !---------------------------------------------
       if(mpirank/=0) then
         call mpi_send(nselect,1,mpi_integer8,0,5,mpi_comm_world,status,mpierr)
       else
         mpik=nselect
         do mpii=1,mpisize-1
           call mpi_recv(mpij,1,mpi_integer8,mpii,5,mpi_comm_world,status,mpierr)
           mpik=mpik+mpij  ! count the total number of selected features
         end do
       end if
       !----

       ! delete useless array
       if((icomb+1)==rung) then
         deallocate(fout)
         deallocate(name_fout)
         deallocate(lastop_out)
         deallocate(dimout)
       end if
       !---

        !synchronize and broadcast
        call mpi_barrier(mpi_comm_world,mpierr)
        if(mpirank/=0) then
           do i=1,mpisize-1
             if(mpirank>1) call mpi_recv(j,1,mpi_integer8,mpirank-1,1,mpi_comm_world,status,mpierr)
             if(ntot>ubound(fin,2)) call addm_in(ntot-ubound(fin,2))
             if(mpirank<mpisize-1) call mpi_send(j,1,mpi_integer8,mpirank+1,1,mpi_comm_world,status,mpierr)
           end do
        end if

        i=nthis/10000
        j=mod(nthis,int8(10000))
        k=ntot-nthis
        do mpii=1,i
        call mpi_bcast(fin(:,k+(mpii-1)*10000+1:k+mpii*10000),sum(nsample)*10000,mpi_double_precision,0,&
                       mpi_comm_world,mpierr)
         call mpi_bcast(lastop_in(k+(mpii-1)*10000+1:k+mpii*10000),10000*10,mpi_character,0,mpi_comm_world,mpierr)
        call mpi_bcast(name_fin(k+(mpii-1)*10000+1:k+mpii*10000),10000*lname,mpi_character,0,mpi_comm_world,mpierr)
        call mpi_bcast(dimin(:,k+(mpii-1)*10000+1:k+mpii*10000),ndimtype*10000,mpi_double_precision,0,&
                       mpi_comm_world,mpierr)
        end do
        call mpi_bcast(fin(:,ntot-j+1:ntot),sum(nsample)*j,mpi_double_precision,0,mpi_comm_world,mpierr)
        call mpi_bcast(lastop_in(ntot-j+1:ntot),j*10,mpi_character,0,mpi_comm_world,mpierr)
        call mpi_bcast(name_fin(ntot-j+1:ntot),j*lname,mpi_character,0,mpi_comm_world,mpierr)
        call mpi_bcast(dimin(:,ntot-j+1:ntot),ndimtype*j,mpi_double_precision,0,mpi_comm_world,mpierr)

        ! vector features
        if(nvf>0) then
           if(mpirank/=0) then
              do i=1,mpisize-1
                if(mpirank>1) call mpi_recv(j,1,mpi_integer8,mpirank-1,1,mpi_comm_world,status,mpierr)
                if(nvf>ubound(vfeat,3)) call addm_vf(nvf-ubound(vfeat,3))
                if(mpirank<mpisize-1) call mpi_send(j,1,mpi_integer8,mpirank+1,1,mpi_comm_world,status,mpierr)
              end do
           end if

           k=int(10000/vfsize)
           i=nvf/k
           j=mod(nvf,k)
           do mpii=1,i
              call mpi_bcast(vfeat(:,:,(mpii-1)*k+1:mpii*k),sum(nsample)*vfsize*k,mpi_double_precision,0,&
                              mpi_comm_world,mpierr)
           end do
           call mpi_bcast(vfeat(:,:,nvf-j+1:nvf),sum(nsample)*vfsize*j,mpi_double_precision,0,mpi_comm_world,mpierr)
        end if
      
        call mpi_barrier(mpi_comm_world,mpierr)
        if(mpirank==0) then
          write(phiname,'(a,i2.2)') 'phi',icomb
          call writeout(phiname,mpik,nvf,ntot)
        end if

      deallocate(order)
      deallocate(available)
      deallocate(ftag)

  END IF

end do
! -------- end of feature combination ------

! release more space
deallocate(fin)
deallocate(name_fin)
deallocate(lastop_in)
deallocate(dimin)
deallocate(vfeat)
if(nvf_init>0) deallocate(vfname)
!---

!---------------------------------------
! create and broadcast neach
!---------------------------------------

if(rung==0) then
 neach=nselect
else
 if(mpirank/=0) then
     call mpi_send(nf(rung),1,mpi_integer8,0,1,mpi_comm_world,status,mpierr)
     call mpi_send(nselect,1,mpi_integer8,0,2,mpi_comm_world,status,mpierr)
     call mpi_send(nvf-nvf_old,1,mpi_integer8,0,3,mpi_comm_world,status,mpierr)
 else
      neach(1)=nselect
      nvfeach(1)=nvf
      do mpii=1,mpisize-1
           call mpi_recv(mpij,1,mpi_integer8,mpii,1,mpi_comm_world,status,mpierr)
           nf(rung)=nf(rung)+mpij
           call mpi_recv(neach(mpii+1),1,mpi_integer8,mpii,2,mpi_comm_world,status,mpierr)
           call mpi_recv(nvfeach(mpii+1),1,mpi_integer8,mpii,3,mpi_comm_world,status,mpierr)
      end do
     write(phiname,'(a,i2.2)') 'phi',rung
     write(*,'(a,i15)') 'Number of newly generated features: ',nf(rung)
     call writeout(phiname,sum(neach),sum(nvfeach),ntot+nf(rung))
 end if
end if

! Broadcast 
call mpi_bcast(neach,mpisize,mpi_integer8,0,mpi_comm_world,mpierr)
call mpi_barrier(mpi_comm_world,mpierr)
!-----------------------------------------------------------------------------


! -------------------------------------
! redundant check for selected features
!---------------------------------------

allocate(available(neach(mpirank+1)))
available=.true.
allocate(order(neach(mpirank+1)+1))

call dup_scheck(neach(mpirank+1),ftag_select,name_fselected,order,available)
if(mpisize>1) call dup_pcheck(neach,ftag_select,name_fselected,order,available)

!---------------------------------------------------------------------------
! inner iterative sure independent screening
!----------------------------------------------------------------------------
if(mpirank==0) then
    allocate(sisfeat(sum(nsample),subs_sis))
    allocate(name_sisfeat(subs_sis))
end if

call iter_sis_p(neach,available,ftag_select,name_fselected,fselected,subs_sis,nout_sis,sisfeat,name_sisfeat)
!----

!--------------------------------------
!--------------------------------------
! final feature space output
!--------------------------------------
!--------------------------------------

if(mpirank==0) then
   
   write(99,'(/a,"   ",a)') 'The best feature is: ',trim(name_sisfeat(1))


   !------
   inquire(file='y0.dat',exist=ly0)
   if(ly0) then
       open(2,file='y0.dat',status='old')
       read(2,*)
       do i=1,sum(nsample)
          read(2,'(a)') line
          j=index(line,trim(adjustl(cname(i))))
          k=index(line(j:),' ')
          read(line(j+k-1:),*) y0(i,:)
       end do
       close(2)
   else
       y0=trainy
   end if
   !-----

   open(2,file='task.fname',status='replace')

   if(trim(adjustl(ptype))=='quanti') then
      do i=1,nout_sis
        tmp_score=sis_score(sisfeat(:,i),y0,ntask_nc,ntask_1c,'not_centered')
        write(2,'(a,i8,a,f12.4)') trim(name_sisfeat(i)),i,'  corr=',tmp_score(1)
      end do
   else if(trim(adjustl(ptype))=='quali') then
      do i=1,nout_sis
        tmp_score=sis_score(sisfeat(:,i),y0,ntask_nc,ntask_1c,'centered') ! for classification
        tmp_score(1)=1.d0/tmp_score(1)-1.d0   ! score 1: overlap_n, score 2: normalized overlap_length
        if( nint(tmp_score(1))/=0 ) then  ! overlapped
           tmp_score(2)=1.d0/tmp_score(2)-1.d0  ! overlapped length
        else   
           tmp_score(2)=-tmp_score(2)  ! separated distance
        end if

        write(2,'(a,i8,a,i6,a,f12.4)') trim(name_sisfeat(i)),i,'  N=',nint(tmp_score(1)),'  S=',tmp_score(2)
      end do
   end if

   close(2)

   k=0
   do i=1,ntask_nc
   do j=1,ntask_1c
      k=k+1
      mm1=sum(nsample(:j-1))+1
      mm2=sum(nsample(:j))
      write(line,'(a,i3.3,a)') 'task',k,'.dat'
      open(3,file=trim(adjustl(line)),status='replace')

      if(trim(adjustl(ptype))=='quanti') then
1006  format(*(e15.5))
1007  format(*(e15.5))
        do ll=mm1,mm2
          write(3,1006) trainy(ll,i),sisfeat(ll,:nout_sis)
        end do
      else
        do ll=mm1,mm2
          write(3,1007) sisfeat(ll,:nout_sis)  ! no y value for qualitative
        end do
      end if

      close(3)
   end do
   end do

   write(*,'(a,i10/)') 'Size of the selected subspace by SIS: ',nout_sis
   write(99,'(a,i10)') 'Size of the selected subspace by SIS: ',nout_sis
  
   etime=mpi_wtime()
   write(99,'(/a)')   '----------------------------------------------------------------------------------'
   write(99,'(a,f15.2)') 'Total time spent (second): ',etime-stime
   write(99,'(/a/)') '                                               Have a nice day !    '
   close(99)

end if

! release all the space
deallocate(trainy)
deallocate(trainy_c)
deallocate(y0)
deallocate(sfeat)
deallocate(sfname)
deallocate(cname)
deallocate(fselected)
deallocate(ftag_select)
deallocate(score_select)
deallocate(name_fselected)
deallocate(mpin)
deallocate(neach)
deallocate(available)
deallocate(SD)
deallocate(nsample)
deallocate(ngroup)
if(mpirank==0) then
deallocate(sisfeat)
deallocate(name_sisfeat)
end if
deallocate(tag)
deallocate(nvfeach)
if(lreject) deallocate(reject)

call mpi_finalize(mpierr)

contains

function get_max(ar)
integer ii,get_max
character(len=150) maxi
character(len=*)  ar(:)
maxi= ar(1)
get_max= 1
do ii=2, size(ar)
  if (ar(ii)>maxi) then
  maxi = ar(ii)
  get_max = ii
  end if
end do
end function


subroutine combine(fin1,name_fin1,lastop_in1,dimin1,fin2,name_fin2,lastop_in2,dimin2,foutsave,nf,op)
! input: fin1,name_fin1,lastop_in1,dimin1,fin2,name_fin2,lastop_in2,dimin2,foutsave,op
! output: nf,fout,name_fout,lastop_out,dimout,fselected,name_fselected,...
! lastop_in1,lastop_in2: the last operator applied in a feature
! fin1,name_fin1,fin2,name_fin2: the two feature and feature name space to be combined
! dimin1,dimin2: dimension (unit) of each feature in fin1 and fin2
! nf: total number of feature from this combination
! foutsave: store the combined features to the fout?
! op: operators

implicit none
real*8 fin1(:,:),fin2(:,:),tmp(ubound(fin1,1)),&
       dimin1(:,:),dimin2(:,:),dimtmp(ubound(dimin1,1)),tmp_vf(ubound(fin1,1),vfsize)
real progress
integer*8 nfin1,nfin2,i,j,k,l,nf,ns,counter,ntotal
character(len=*) name_fin1(:),name_fin2(:),op,lastop_in1(:),lastop_in2(:)
character name_tmp*150,lastop_tmp*10
logical foutsave

ns=ubound(fin1,1)
nfin1=ubound(fin1,2)  ! last generated
nfin2=ubound(fin2,2)  ! total
nf=0  ! for this subspace
progress=0.2
counter=0
ntotal=nfin1*(nfin2-sum(mpin))+(nfin1-1)*nfin1/2.0+nfin1*(sum(mpin)-sum(mpin(:mpirank+1)))+nfin1

do i=1,nfin1

   counter=counter+1
! no operation
      IF(trim(adjustl(op))=='NO') THEN
          lastop_tmp=''
          name_tmp='('//trim(adjustl(name_fin1(i)))//')'
          dimtmp=dimin1(:,i)
        if( isscalar(name_fin1(i)) ) then  
             tmp=fin1(:,i)
             call isgoodf(tmp,name_tmp,lastop_tmp,dimtmp,nf,foutsave)
        else
             tmp_vf=vfeat(:,:,nint(fin1(1,i)))
             call isgoodvf(tmp_vf,name_tmp,lastop_tmp,dimtmp,nf,nvf,foutsave)
        end if
        cycle
      END IF

! unary operators
      ! exp
      IF(index(op,'(exp)')/=0 ) then
      if(  index(lastop_in1(i),'(exp')==0 .and. index(lastop_in1(i),'(log)')==0 ) then ! avoid exp(exp( and exp(log(
        lastop_tmp='(exp)'
        name_tmp='exp('//trim(adjustl(name_fin1(i)))//')'
        dimtmp=dimcomb(dimin1(:,i),dimin1(:,i),'(exp)')
        if( isscalar(name_fin1(i)) ) then  ! vector feature or not
             tmp=exp(fin1(:,i))
             call isgoodf(tmp,name_tmp,lastop_tmp,dimtmp,nf,foutsave)
        else
             tmp_vf=exp(vfeat(:,:,nint(fin1(1,i))))
             call isgoodvf(tmp_vf,name_tmp,lastop_tmp,dimtmp,nf,nvf,foutsave)
        end if
      end if
      END IF

      ! exp-
      IF(index(op,'(exp-)')/=0 ) then
      if( index(lastop_in1(i),'(exp')==0  .and. index(lastop_in1(i),'(log)')==0 ) then ! avoid exp(exp( and exp(log(
        lastop_tmp='(exp-)'
        name_tmp='exp(-'//trim(adjustl(name_fin1(i)))//')'
        dimtmp=dimcomb(dimin1(:,i),dimin1(:,i),'(exp-)')
        if( isscalar(name_fin1(i)) ) then  ! vector feature or not
             tmp=exp(-fin1(:,i))
             call isgoodf(tmp,name_tmp,lastop_tmp,dimtmp,nf,foutsave)
        else
             tmp_vf=exp(-vfeat(:,:,nint(fin1(1,i))))
             call isgoodvf(tmp_vf,name_tmp,lastop_tmp,dimtmp,nf,nvf,foutsave)
        end if
      end if
      END IF

      ! ^-1
      IF(index(op,'(^-1)')/=0) then
        if(minval(abs(fin1(:,i)))>1d-50 ) then
           lastop_tmp='(^-1)'
           name_tmp='('//trim(adjustl(name_fin1(i)))//')^-1'
           dimtmp=dimcomb(dimin1(:,i),dimin1(:,i),'(^-1)')
           if( isscalar(name_fin1(i)) ) then  ! vector feature or not
                tmp=(fin1(:,i))**(-1)
                call isgoodf(tmp,name_tmp,lastop_tmp,dimtmp,nf,foutsave)
           else
                tmp_vf=(vfeat(:,:,nint(fin1(1,i))))**(-1)
                call isgoodvf(tmp_vf,name_tmp,lastop_tmp,dimtmp,nf,nvf,foutsave)
           end if
        end if
       END IF

      ! SCD: Standard Cauchy Distribution
      IF(index(op,'(SCD)')/=0) then
           lastop_tmp='(SCD)'
           name_tmp='SCD('//trim(adjustl(name_fin1(i)))//')'
           dimtmp=dimcomb(dimin1(:,i),dimin1(:,i),'(SCD)')
           if( isscalar(name_fin1(i)) ) then  ! vector feature or not
                tmp=1.0d0/(PI*(1.0d0+(fin1(:,i))**2))
                call isgoodf(tmp,name_tmp,lastop_tmp,dimtmp,nf,foutsave)
           else
                tmp_vf=1.0d0/(PI*(1.0d0+(vfeat(:,:,nint(fin1(1,i))))**2))
                call isgoodvf(tmp_vf,name_tmp,lastop_tmp,dimtmp,nf,nvf,foutsave)
           end if
       END IF

      ! ^2
      IF(index(op,'(^2)')/=0) then
        if(index(lastop_in1(i),'(sqrt)')==0 ) then ! avoid (sqrt())^2
           lastop_tmp='(^2)'
           name_tmp='('//trim(adjustl(name_fin1(i)))//')^2'
           dimtmp=dimcomb(dimin1(:,i),dimin1(:,i),'(^2)')
           if( isscalar(name_fin1(i)) ) then  ! vector feature or not
                tmp=(fin1(:,i))**2
                call isgoodf(tmp,name_tmp,lastop_tmp,dimtmp,nf,foutsave)
           else
                tmp_vf=(vfeat(:,:,nint(fin1(1,i))))**2
                call isgoodvf(tmp_vf,name_tmp,lastop_tmp,dimtmp,nf,nvf,foutsave)
           end if
        end if
       END IF

      ! ^3
      IF(index(op,'(^3)')/=0) then
        if(index(lastop_in1(i),'(cbrt)')==0 ) then ! avoid (cbrt())^3
         lastop_tmp='(^3)'
         name_tmp='('//trim(adjustl(name_fin1(i)))//')^3'
         dimtmp=dimcomb(dimin1(:,i),dimin1(:,i),'(^3)')
         if(  isscalar(name_fin1(i)) ) then  ! vector feature or not
              tmp=(fin1(:,i))**3
              call isgoodf(tmp,name_tmp,lastop_tmp,dimtmp,nf,foutsave)
         else
              tmp_vf=(vfeat(:,:,nint(fin1(1,i))))**3
              call isgoodvf(tmp_vf,name_tmp,lastop_tmp,dimtmp,nf,nvf,foutsave)
         end if
       end if
     END IF

      ! ^6
      IF(index(op,'(^6)')/=0) then
         lastop_tmp='(^6)'
         name_tmp='('//trim(adjustl(name_fin1(i)))//')^6'
         dimtmp=dimcomb(dimin1(:,i),dimin1(:,i),'(^6)')
         if( isscalar(name_fin1(i)) ) then  ! vector feature or not
              tmp=(fin1(:,i))**6
              call isgoodf(tmp,name_tmp,lastop_tmp,dimtmp,nf,foutsave)
         else
              tmp_vf=(vfeat(:,:,nint(fin1(1,i))))**6
              call isgoodvf(tmp_vf,name_tmp,lastop_tmp,dimtmp,nf,nvf,foutsave)
         end if
       END IF

      ! sqrt
      IF(index(op,'(sqrt)')/=0) then
        if(index(lastop_in1(i),'(^2)')==0 ) then  ! avoid sqrt((^2))
          if( minval(fin1(:,i))>0 .and. (isscalar(name_fin1(i)))  ) then
              lastop_tmp='(sqrt)'     
              name_tmp='sqrt('//trim(adjustl(name_fin1(i)))//')'
              dimtmp=dimcomb(dimin1(:,i),dimin1(:,i),'(sqrt)')
              tmp=sqrt(fin1(:,i))
              call isgoodf(tmp,name_tmp,lastop_tmp,dimtmp,nf,foutsave)
          else if ( .not. isscalar(name_fin1(i)) )  then
              if(minval(vfeat(:,:,nint(fin1(1,i))))>0) then
              lastop_tmp='(sqrt)'
              name_tmp='sqrt('//trim(adjustl(name_fin1(i)))//')'
              dimtmp=dimcomb(dimin1(:,i),dimin1(:,i),'(sqrt)')
              tmp_vf=sqrt(vfeat(:,:,nint(fin1(1,i))))
              call isgoodvf(tmp_vf,name_tmp,lastop_tmp,dimtmp,nf,nvf,foutsave)
              end if
          end if
         end if
       END IF

      ! cbrt: cube root
      IF(index(op,'(cbrt)')/=0) then
        if(index(lastop_in1(i),'(^3)')==0 ) then  ! avoid cbrt((^3))
          if( isscalar(name_fin1(i)) ) then
              lastop_tmp='(cbrt)'
              name_tmp='cbrt('//trim(adjustl(name_fin1(i)))//')'
              dimtmp=dimcomb(dimin1(:,i),dimin1(:,i),'(cbrt)')
              tmp=(fin1(:,i))**(1.d0/3.d0)
              call isgoodf(tmp,name_tmp,lastop_tmp,dimtmp,nf,foutsave)
          else if ( .not. isscalar(name_fin1(i)) )  then
              lastop_tmp='(cbrt)'
              name_tmp='cbrt('//trim(adjustl(name_fin1(i)))//')'
              dimtmp=dimcomb(dimin1(:,i),dimin1(:,i),'(cbrt)')
              tmp_vf=(vfeat(:,:,nint(fin1(1,i))))**(1.d0/3.d0)
              call isgoodvf(tmp_vf,name_tmp,lastop_tmp,dimtmp,nf,nvf,foutsave)
          end if
         end if
       END IF

      ! log
      IF(index(op,'(log)')/=0) then
        if( index(lastop_in1(i),'(exp')==0 .and. index(lastop_in1(i),'(log)')==0  ) then ! avoid log(exp( and log(log(
          if( minval(fin1(:,i))>0 .and. (isscalar(name_fin1(i)))  ) then
              lastop_tmp='(log)'
              name_tmp='log('//trim(adjustl(name_fin1(i)))//')'
              dimtmp=dimcomb(dimin1(:,i),dimin1(:,i),'(log)')
              tmp=log(fin1(:,i))
              call isgoodf(tmp,name_tmp,lastop_tmp,dimtmp,nf,foutsave)
          else if ( .not. isscalar(name_fin1(i)) )  then
              if(minval(vfeat(:,:,nint(fin1(1,i))))>0) then
              lastop_tmp='(log)'
              name_tmp='log('//trim(adjustl(name_fin1(i)))//')'
              dimtmp=dimcomb(dimin1(:,i),dimin1(:,i),'(log)')
              tmp_vf=log(vfeat(:,:,nint(fin1(1,i))))
              call isgoodvf(tmp_vf,name_tmp,lastop_tmp,dimtmp,nf,nvf,foutsave)
              end if
          end if
         end if
       END IF

      ! sin
      IF(index(op,'(sin)')/=0) then
         lastop_tmp='(sin)'
         name_tmp='sin('//trim(adjustl(name_fin1(i)))//')'
         dimtmp=dimcomb(dimin1(:,i),dimin1(:,i),'(sin)')
         if(  isscalar(name_fin1(i)) ) then  ! vector feature or not
              tmp=sin(fin1(:,i))
              call isgoodf(tmp,name_tmp,lastop_tmp,dimtmp,nf,foutsave)
         else
              tmp_vf=sin(vfeat(:,:,nint(fin1(1,i))))
              call isgoodvf(tmp_vf,name_tmp,lastop_tmp,dimtmp,nf,nvf,foutsave)
         end if
     END IF

      ! cos
      IF(index(op,'(cos)')/=0) then
         lastop_tmp='(cos)'
         name_tmp='cos('//trim(adjustl(name_fin1(i)))//')'
         dimtmp=dimcomb(dimin1(:,i),dimin1(:,i),'(cos)')
         if(  isscalar(name_fin1(i)) ) then  ! vector feature or not
              tmp=cos(fin1(:,i))
              call isgoodf(tmp,name_tmp,lastop_tmp,dimtmp,nf,foutsave)
         else
              tmp_vf=cos(vfeat(:,:,nint(fin1(1,i))))
              call isgoodvf(tmp_vf,name_tmp,lastop_tmp,dimtmp,nf,nvf,foutsave)
         end if
     END IF


  ! binary operators
  do j=1,nfin2  

       ! fin1=An-1; fin2=A0+A1+...+An-1
       if(mpion .eqv. .false. .and. j>(nfin2-nfin1) .and. j<=nfin2-nfin1+i) cycle
       if(mpion .and. j>(nfin2-sum(mpin)) .and. j<=(nfin2-sum(mpin)+sum(mpin(:mpirank))+i)) cycle  
       counter=counter+1
      
      ! sum and subtract 

      ! homogeneous combination
       IF(index(op,'(+)')/=0 .or. index(op,'(-)')/=0  .or. index(op,'(|-|)')/=0   ) THEN

          ! for + and -, exp() functions are only allowed to combine with exp(), and is so for log, SCD
          if( (index(lastop_in1(i),'(exp')==0 .and. index(lastop_in2(j),'(exp')/=0)    .or. &
              (index(lastop_in1(i),'(exp')/=0 .and. index(lastop_in2(j),'(exp')==0)    .or. &
              (index(lastop_in1(i),'(log)')==0 .and. index(lastop_in2(j),'(log)')/=0)  .or. &
              (index(lastop_in1(i),'(log)')/=0 .and. index(lastop_in2(j),'(log)')==0)  .or. &
              (index(lastop_in1(i),'(SCD)')==0 .and. index(lastop_in2(j),'(SCD)')/=0)  .or. &
              (index(lastop_in1(i),'(SCD)')/=0 .and. index(lastop_in2(j),'(SCD)')==0) ) goto 600

       if ( maxval(abs(dimin1(:,i)-dimin2(:,j)))<=1d-10 ) then
               IF(index(op,'(+)')/=0) then
                   lastop_tmp='(+)'
                   name_tmp='('//trim(adjustl(name_fin1(i)))//'+'//trim(adjustl(name_fin2(j)))//')'
                   dimtmp=dimcomb(dimin1(:,i),dimin2(:,j),'(+)')
                  if( isscalar(name_fin1(i)) .and. isscalar(name_fin2(j))  ) then 
                       tmp=fin1(:,i)+fin2(:,j)
                       call isgoodf(tmp,name_tmp,lastop_tmp,dimtmp,nf,foutsave)
                  else 
                 !    if(  .not. isscalar(name_fin1(i))  .and. isscalar(name_fin2(j))  )then 
                 !      tmp=sum(vfeat(:,:,nint(fin1(1,i))),2)+fin2(:,j)
                 !      call isgoodf(tmp,name_tmp,lastop_tmp,dimtmp,nf,foutsave)
                 !    else if( isscalar(name_fin1(i)) .and. (.not. isscalar(name_fin2(j))) )then
                 !      tmp=fin1(:,i)+sum(vfeat(:,:,nint(fin2(1,j))),2)
                 !      call isgoodf(tmp,name_tmp,lastop_tmp,dimtmp,nf,foutsave)
                      if (  (.not. isscalar(name_fin1(i)))  .and. (.not. isscalar(name_fin2(j))) ) then
                        tmp_vf=vfeat(:,:,nint(fin1(1,i)))+vfeat(:,:,nint(fin2(1,j)))
                      end if
                        call isgoodvf(tmp_vf,name_tmp,lastop_tmp,dimtmp,nf,nvf,foutsave)
                  end if

                END IF

                IF(index(op,'(-)')/=0) then
                  lastop_tmp='(-)'
                  name_tmp='('//trim(adjustl(name_fin1(i)))//'-'//trim(adjustl(name_fin2(j)))//')'
                  dimtmp=dimcomb(dimin1(:,i),dimin2(:,j),'(-)')
                  if( isscalar(name_fin1(i)) .and. isscalar(name_fin2(j))  ) then 
                       tmp=fin1(:,i)-fin2(:,j)
                       call isgoodf(tmp,name_tmp,lastop_tmp,dimtmp,nf,foutsave)
                  else 
                 !      if( (.not. isscalar(name_fin1(i)))  .and. isscalar(name_fin2(j)) ) then
                 !         tmp=sum(vfeat(:,:,nint(fin1(1,i))),2)-fin2(:,j)
                 !         call isgoodf(tmp,name_tmp,lastop_tmp,dimtmp,nf,foutsave)
                 !     else if( isscalar(name_fin1(i)) .and. (.not. isscalar(name_fin2(j))) )then
                 !         tmp=fin1(:,i)-sum(vfeat(:,:,nint(fin2(1,j))),2)
                 !         call isgoodf(tmp,name_tmp,lastop_tmp,dimtmp,nf,foutsave)
                       if (  (.not. isscalar(name_fin1(i)))  .and. (.not. isscalar(name_fin2(j)))  ) then
                           tmp_vf=vfeat(:,:,nint(fin1(1,i)))-vfeat(:,:,nint(fin2(1,j)))
                        end if
                        call isgoodvf(tmp_vf,name_tmp,lastop_tmp,dimtmp,nf,nvf,foutsave)
                   end if

                 END IF

                IF(index(op,'(|-|)')/=0) then  ! absolute difference
                  lastop_tmp='(|-|)'
                  name_tmp='abs('//trim(adjustl(name_fin1(i)))//'-'//trim(adjustl(name_fin2(j)))//')'
                  dimtmp=dimcomb(dimin1(:,i),dimin2(:,j),'(|-|)')
                  if( isscalar(name_fin1(i)) .and. isscalar(name_fin2(j))  ) then
                       tmp=abs(fin1(:,i)-fin2(:,j))
                       call isgoodf(tmp,name_tmp,lastop_tmp,dimtmp,nf,foutsave)
                  else
                 !      if( (.not. isscalar(name_fin1(i)))  .and. isscalar(name_fin2(j)) ) then
                 !         tmp=abs(sum(vfeat(:,:,nint(fin1(1,i))),2)-fin2(:,j))
                 !         call isgoodf(tmp,name_tmp,lastop_tmp,dimtmp,nf,foutsave)
                 !     else if( isscalar(name_fin1(i)) .and. (.not. isscalar(name_fin2(j))) )then
                 !         tmp=abs(fin1(:,i)-sum(vfeat(:,:,nint(fin2(1,j))),2))
                 !         call isgoodf(tmp,name_tmp,lastop_tmp,dimtmp,nf,foutsave)
                      if (  (.not. isscalar(name_fin1(i)))  .and. (.not. isscalar(name_fin2(j)))  ) then
                          tmp_vf=abs(vfeat(:,:,nint(fin1(1,i)))-vfeat(:,:,nint(fin2(1,j))))
                       end if
                       call isgoodvf(tmp_vf,name_tmp,lastop_tmp,dimtmp,nf,nvf,foutsave)
                  end if

                 END IF

         end if
         END IF

600   continue

      ! multiplication and division
           if( (index(lastop_in1(i),'(exp)')/=0 .and. index(lastop_in2(j),'(exp)')/=0) .or. &
               (index(lastop_in1(i),'(exp-)')/=0 .and. index(lastop_in2(j),'(exp-)')/=0) .or. &
               (index(lastop_in1(i),'(log)')/=0 .and. index(lastop_in2(j),'(log)')/=0)  .or. &
               (index(lastop_in1(i),'(SCD)')/=0 .and. index(lastop_in2(j),'(SCD)')/=0) ) goto 601

           !multiplication
            IF(index(op,'(*)')/=0) then
                lastop_tmp='(*)'
                name_tmp='('//trim(adjustl(name_fin1(i)))//'*'//trim(adjustl(name_fin2(j)))//')'
                dimtmp=dimcomb(dimin1(:,i),dimin2(:,j),'(*)')
               if( isscalar(name_fin1(i)) .and. isscalar(name_fin2(j)) ) then 
                    tmp=fin1(:,i)*fin2(:,j)
                    call isgoodf(tmp,name_tmp,lastop_tmp,dimtmp,nf,foutsave)
               else 
                    if( (.not. isscalar(name_fin1(i)))  .and. isscalar(name_fin2(j))  ) then
                       do k=1,vfsize
                          tmp_vf(:,k)=vfeat(:,k,nint(fin1(1,i)))*fin2(:,j)
                       end do
                    else if(isscalar(name_fin1(i)) .and. (.not. isscalar(name_fin2(j))) ) then
                       do k=1,vfsize
                          tmp_vf(:,k)=fin1(:,i)*vfeat(:,k,nint(fin2(1,j)))
                       end do
                    else if (  (.not. isscalar(name_fin1(i)))  .and. (.not. isscalar(name_fin2(j))) ) then
                       tmp_vf=vfeat(:,:,nint(fin1(1,i)))*vfeat(:,:,nint(fin2(1,j)))
                    end if
                    call isgoodvf(tmp_vf,name_tmp,lastop_tmp,dimtmp,nf,nvf,foutsave)
               end if
            END IF

          ! division
           IF(index(op,'(/)')/=0) then
                   lastop_tmp='(/)'
                   name_tmp='('//trim(adjustl(name_fin1(i)))//'/'//trim(adjustl(name_fin2(j)))//')'
                   dimtmp=dimcomb(dimin1(:,i),dimin2(:,j),'(/)')
              if(index(lastop_in2(j),'(/)')==0 .and. &
              (.not. (index(name_fin1(i),trim(adjustl(name_fin2(j))))/=0 .and. index(lastop_in1(i),'(*)')/=0)) ) then
              ! avoid A/(B/C) and A*B/B)
                  if( isscalar(name_fin1(i)) .and. isscalar(name_fin2(j))  ) then
                       if(minval(abs(fin2(:,j)))>1d-50 ) then 
                       tmp=fin1(:,i)/fin2(:,j)
                       call isgoodf(tmp,name_tmp,lastop_tmp,dimtmp,nf,foutsave)
                       end if
                  else 
                       if( (.not. isscalar(name_fin1(i)))  .and. isscalar(name_fin2(j)) ) then
                          if(minval(abs(fin2(:,j)))>1d-50) then
                            do k=1,vfsize
                               tmp_vf(:,k)=vfeat(:,k,nint(fin1(1,i)))/fin2(:,j)
                            end do
                          end if
                     else if(isscalar(name_fin1(i)) .and. (.not. isscalar(name_fin2(j))) )then
                          if(minval(abs(vfeat(:,:,nint(fin2(1,j)))))>1d-50) then
                            do k=1,vfsize
                              tmp_vf(:,k)=fin1(:,i)/vfeat(:,k,nint(fin2(1,j)))
                            end do
                          end if
                     else if (  (.not. isscalar(name_fin1(i)))  .and. (.not. isscalar(name_fin2(j)))  ) then
                          if(minval(abs(vfeat(:,:,nint(fin2(1,j)))))>1d-50 ) then
                          tmp_vf=vfeat(:,:,nint(fin1(1,i)))/vfeat(:,:,nint(fin2(1,j)))
                          end if
                     end if
                     call isgoodvf(tmp_vf,name_tmp,lastop_tmp,dimtmp,nf,nvf,foutsave)

                  end if
                end if
                 !------

                 lastop_tmp='(/)'
                 name_tmp='('//trim(adjustl(name_fin2(j)))//'/'//trim(adjustl(name_fin1(i)))//')'
                 dimtmp=dimcomb(dimin2(:,j),dimin1(:,i),'(/)')
               if(index(lastop_in1(i),'(/)')==0 .and. &
               (.not. (index(name_fin1(i),trim(adjustl(name_fin2(j))))/=0 .and. index(lastop_in1(i),'(*)')/=0)) ) then
                  if( isscalar(name_fin1(i)) .and.  isscalar(name_fin2(j)) ) then
                       if(minval(abs(fin1(:,i)))>1d-50) then
                       tmp=fin2(:,j)/fin1(:,i)
                       call isgoodf(tmp,name_tmp,lastop_tmp,dimtmp,nf,foutsave)
                       end if
                  else
                       if( (.not. isscalar(name_fin1(i)))  .and.  isscalar(name_fin2(j)) ) then
                          if(minval(abs(vfeat(:,:,nint(fin1(1,i)))))>1d-50) then
                            do k=1,vfsize
                             tmp_vf(:,k)=fin2(:,j)/vfeat(:,k,nint(fin1(1,i)))
                            end do
                          end if
                       elseif( isscalar(name_fin1(i)) .and. (.not. isscalar(name_fin2(j))) )then
                          if(minval(abs(fin1(:,i)))>1d-50) then
                             do k=1,vfsize
                               tmp_vf(:,k)=vfeat(:,k,nint(fin2(1,j)))/fin1(:,i)
                             end do
                          end if
                       else if ( (.not. isscalar(name_fin1(i)))  .and. (.not. isscalar(name_fin2(j)))  ) then
                          if(minval(abs(vfeat(:,:,nint(fin1(1,i)))))>1d-50 ) then
                          tmp_vf=vfeat(:,:,nint(fin2(1,j)))/vfeat(:,:,nint(fin1(1,i)))
                          end if
                       end if
                       call isgoodvf(tmp_vf,name_tmp,lastop_tmp,dimtmp,nf,nvf,foutsave)

                   end if
                end if
           END if

   601   continue

  end do

  if(mpion .and. float(counter)/float(ntotal)>=progress) then
  write(*,'(a,i4,2(a,i15),a,f6.1,a)') 'mpirank= ',mpirank,'  nf=',nf,'  nselect=',nselect,'  progress=',progress*100,'%'
  progress=progress+0.2

  end if

end do

end subroutine


function goodf(feat,name_feat,dimens)
integer*8 i,j,k,nf,l,ll,mm1,mm2
real*8 feat(:),dimens(:),scoretmp(2),maxabs
character(len=*) name_feat
logical goodf,lsame

goodf=.true.
!----------------
! level1 screen
!-----------------

! delete too large or small features
maxabs=maxval(abs(feat))
if(maxabs>1d50 .or. maxabs<=1d-50) then
goodf=.false.
return
end if

! delete unit feat
!do ll=1,ntask_1c
!  mm1=sum(nsample(:ll-1))+1
!  mm2=sum(nsample(:ll))
   mm1=1
   mm2=sum(nsample(:ntask_1c))
  if(maxval(abs(feat(mm1:mm2)-feat(mm1)))<=1d-10) then
    goodf=.false.
    return
  end if
!end do

!-------------------------
!level2 screen for the final space
!-------------------------

if(maxabs>maxfval_ub .or. maxabs<maxfval_lb) return

!sure independent screening
if(trim(adjustl(ptype))=='quanti') scoretmp=sis_score(feat,trainy_c,ntask_nc,ntask_1c,'centered')
if(trim(adjustl(ptype))=='quali')   scoretmp=sis_score(feat,trainy,ntask_nc,ntask_1c,'centered') !classification

if(sis_s_on) then
 if(scoretmp(1)<score_select(subs_sis,1)) return   
end if

! all element should be involved
if(allele) then
if( .not. isABC(trim(name_feat)) ) return
end if

!----
if(lreject) then
  if(name_feat(1:1)==' ') name_feat=adjustl(name_feat)
  lsame=.false.
  i=0; j=nreject; k=i+ceiling((j-i)/2.0)
  do while(i/=j)
     if(trim(name_feat)==trim(reject(k))) then
       lsame=.true.
       i=j
     else if(name_feat<reject(k)) then
       i=k;
     else if(name_feat>reject(k)) then
       j=k;
     end if

     if(k==i+ceiling((j-i)/2.0)) then
         i=j
     else
         k=i+ceiling((j-i)/2.0)
     end if
  end do
  if(lsame) return
end if

!--------------------------
! level1 and level2 PASS !
!--------------------------
nnomb=50000
nselect=nselect+1
if(nselect>ubound(fselected,2)) call addm_select(nnomb)
fselected(:,nselect)=feat
name_fselected(nselect)=name_feat
score_select(nselect,:)=scoretmp
ftag_select(nselect)=abs(sum(tag*feat))/ntask_1c 
if(nselect> nnn ) then
call sis_s(subs_sis,nselect,fselected,name_fselected,ftag_select,score_select)
sis_s_on=.true.
end if

!-----------------

end function

subroutine addm_select(n)
! increase array size
real*8,allocatable:: real2d(:,:),real1d(:)
character(len=lname),allocatable:: char1d(:)
integer*8 i,j,n
i=ubound(fselected,1)
j=ubound(fselected,2)

! fselected
allocate(real2d(i,j))
real2d=fselected
deallocate(fselected)
allocate(fselected(i,j+n))
fselected(:,:j)=real2d
deallocate(real2d)
!-----
! name_fselected
allocate(char1d(j))
char1d=name_fselected
deallocate(name_fselected)
allocate(name_fselected(j+n))
name_fselected(:j)=char1d
deallocate(char1d)
!---
! ftag_select
allocate(real1d(j))
real1d=ftag_select
deallocate(ftag_select)
allocate(ftag_select(j+n))
ftag_select(:j)=real1d
deallocate(real1d)
!-----
! score_select
allocate(real2d(j,2))
real2d=score_select
deallocate(score_select)
allocate(score_select(j+n,2))
score_select(:j,:)=real2d
deallocate(real2d)


end subroutine


subroutine addm_out(n)
! increase array size
real*8,allocatable:: real2d(:,:)
character(len=lname),allocatable:: char1d(:),char1d2(:)*10
integer*8 i,j,k,n
i=ubound(fout,1)
j=ubound(fout,2)

! fout
allocate(real2d(i,j))
real2d=fout
deallocate(fout)
allocate(fout(i,j+n))
fout(:,:j)=real2d
deallocate(real2d)
!-----
!name_fout
allocate(char1d(j))
char1d=name_fout
deallocate(name_fout)
allocate(name_fout(j+n))
name_fout(:j)=char1d
deallocate(char1d)
!----
!lastop_out
allocate(char1d2(j))
char1d2=lastop_out
deallocate(lastop_out)
allocate(lastop_out(j+n))
lastop_out(:j)=char1d2
deallocate(char1d2)
!----

!dimout
i=ubound(dimout,1)
allocate(real2d(i,j))
real2d=dimout
deallocate(dimout)
allocate(dimout(i,j+n))
dimout(:,:j)=real2d
deallocate(real2d)
!--
end subroutine


subroutine addm_in(n)
! increase array size
real*8,allocatable:: real2d(:,:)
character(len=lname),allocatable:: char1d(:),char1d2(:)*10
integer*8 i,j,k,n
i=ubound(fin,1)
j=ubound(fin,2)

! fin
allocate(real2d(i,j))
real2d=fin
deallocate(fin)
allocate(fin(i,j+n))
fin(:,:j)=real2d
deallocate(real2d)
!-----
!name_fin
allocate(char1d(j))
char1d=name_fin
deallocate(name_fin)
allocate(name_fin(j+n))
name_fin(:j)=char1d
deallocate(char1d)
!----
!lastop_in
allocate(char1d2(j))
char1d2=lastop_in
deallocate(lastop_in)
allocate(lastop_in(j+n))
lastop_in(:j)=char1d2
deallocate(char1d2)

!dimin
i=ubound(dimin,1)
allocate(real2d(i,j))
real2d=dimin
deallocate(dimin)
allocate(dimin(i,j+n))
dimin(:,:j)=real2d
deallocate(real2d)
!--
end subroutine

subroutine addm_vf(n)
! increase array size for vector feature
real*8,allocatable:: real3d(:,:,:)
integer*8 i,j,k,n
i=ubound(vfeat,1)
j=ubound(vfeat,2)
k=ubound(vfeat,3)
allocate(real3d(i,j,k))
real3d=vfeat
deallocate(vfeat)
allocate(vfeat(i,j,k+n))
vfeat(:,:,:k)=real3d
deallocate(real3d)
end subroutine


function dimcomb(dim1,dim2,op)
! for unary operator, set dim1 and dim2 the same
real*8 dim1(:),dim2(:),dimcomb(ubound(dim1,1))
character(len=*) op
integer i,j,k

if(trim(adjustl(op))=='(+)' .or. trim(adjustl(op))=='(-)' .or. trim(adjustl(op))=='(|-|)'  ) then
   do i=1,ubound(dim1,1)
      if(abs(dim1(i)-dim2(i))>1d-10) stop 'Dimension combination error: dimension not matched'
   end do
   dimcomb=dim1
else if(trim(adjustl(op))=='(*)') then
   dimcomb=dim1+dim2
else if(trim(adjustl(op))=='(/)') then
   dimcomb=dim1-dim2
else if(trim(adjustl(op))=='(exp)') then   
   dimcomb=0.d0
else if(trim(adjustl(op))=='(exp-)') then
   dimcomb=0.d0
else if(trim(adjustl(op))=='(log)') then
   dimcomb=0.d0
else if(trim(adjustl(op))=='(SCD)') then 
   dimcomb=0.d0
else if(trim(adjustl(op))=='(sin)') then
   dimcomb=0.d0
else if(trim(adjustl(op))=='(cos)') then
   dimcomb=0.d0
else if(trim(adjustl(op))=='(^-1)') then
   dimcomb=dim1*(-1)
else if(trim(adjustl(op))=='(^2)') then
   dimcomb=dim1*2
else if(trim(adjustl(op))=='(^3)') then
   dimcomb=dim1*3
else if(trim(adjustl(op))=='(^6)') then
   dimcomb=dim1*6
else if(trim(adjustl(op))=='(sqrt)') then
   dimcomb=dim1/2.d0
else if(trim(adjustl(op))=='(cbrt)') then
   dimcomb=dim1/3.d0
end if
end function


subroutine writeout(phiname,i,j,k)
integer*8 i,j,k
character(len=*) phiname
if(nvf_init>0) write(*,'(3a,i15)') 'Total Number vector features: ',j
write(*,'(a,i15)') 'Total mumber of selected features by SIS: ',i
write(*,'(3a,i15)') 'Total number of features in the current space ',trim(phiname),':',k
write(99,'(a,i8)'),'Total Featurespace: ',k
write(*,'(a/)') '------------------------------------------------------------------------------------------'
end subroutine


function better_name(name1,name2)
integer better_name
character(len=*) name1,name2
 if(len_trim(name1)<len_trim(name2) .or. &
   (len_trim(name1)==len_trim(name2) .and. trim(name1)<=trim(name2) ) ) then
    better_name=1
 else
    better_name=2
 end if
end function


subroutine update_availability(available,pavailable)
logical available(:),pavailable(:)
if( any(available) ) then
   pavailable(mpirank+1)=.true.
else
   pavailable(mpirank+1)=.false.
end if
end subroutine


subroutine dup_pcheck(neach,ftag,fname,order,available)
real*8 ftag(:),time_start,time_end
integer*8 i,j,k,l,ll,mpii,mpij,neach(:),mpim(mpisize),mpiloc(mpisize),order(:),loc(1)
character fname(:)*150
logical available(:)
real*8,allocatable:: compftag(:)
character(len=lname),allocatable:: compname(:)
integer mpierr,mpirank,mpisize,status(MPI_STATUS_SIZE)
common mpierr,mpirank,mpisize

IF(mpisize>1) THEN
   ! rank the core in descending order of the total number of features
   mpim=neach
   do i=1,mpisize
      loc(1:1)=maxloc(mpim)
      mpiloc(i)=loc(1)
      mpim(loc(1))=-1
   end do

!   if(mpirank==0)  write(*,'(a)') 'Redundant check between cores starts ... '
   do k=1,mpisize-1

     ! time recording
     if(mpirank==0) time_start=mpi_wtime()
     if(neach(mpiloc(k))>0) then
         ! allocate and assignment
         allocate(compftag(neach(mpiloc(k))))
         allocate(compname(neach(mpiloc(k))))

         if(mpirank==mpiloc(k)-1) then
           compftag=ftag(:neach(mpiloc(k)))
           compname=fname(:neach(mpiloc(k)))
         end if

         !broadcast compftag
          call mpi_bcast(compftag,neach(mpiloc(k)),mpi_double_precision,mpiloc(k)-1,mpi_comm_world,mpierr)
         ! broadcast name
          call mpi_bcast(compname,neach(mpiloc(k))*lname,mpi_character,mpiloc(k)-1,mpi_comm_world,mpierr)

         ! do the comparision
         if( all( mpirank/=(mpiloc(:k)-1) ) .and. neach(mpirank+1)>0 ) then
            do mpij=1,neach(mpiloc(k))
              l=0; ll=order(neach(mpirank+1)+1);
              i=l+ceiling(float(ll-l)/2.0)

               124 continue
              if(abs(compftag(mpij)-ftag(order(i)))<=1d-10 ) then 
              ! if equal
                    if(better_name(compname(mpij),fname(order(i)))==1) then
                       available(order(i))=.false.
                    else
                       compftag(mpij)=0.0
                    end if
                 cycle
              else   ! if not equal
                if(compftag(mpij)>ftag(order(i))) then
                   ll=i
                else
                    l=i
                end if
                if(i==l+ceiling(float(ll-l)/2.0)) cycle
                 i=l+ceiling(float(ll-l)/2.0)
                 goto 124
              end if
            end do
            call mpi_send(compftag,neach(mpiloc(k)),mpi_double_precision,mpiloc(k)-1,33,mpi_comm_world,status,mpierr)

          else if(mpirank==mpiloc(k)-1) then
            do l=1,mpisize
             if( any( l==mpiloc(:k) ) .or. neach(l)==0 ) cycle
            call mpi_recv(compftag,neach(mpiloc(k)),mpi_double_precision,mpi_any_source,33,mpi_comm_world,status,mpierr)
               do i=1,neach(mpiloc(k))
                if(abs(compftag(i))<1d-10) available(i)=.false.
               end do
            end do
         end if

         ! deallocate
         deallocate(compftag)
         deallocate(compname)
     end if

      call mpi_barrier(mpi_comm_world,mpierr)

     ! if(mpirank==0) then
     ! time_end=mpi_wtime()
     ! write(*,'(a,i5,a,i4.4,a,f15.2)') &
     !  'Time (s) spent on block ',k,'/',mpisize-1,'   is:',(time_end-time_start)
     ! end if

  end do

  j=0
  do i=1,neach(mpirank+1)
   if(available(i)) j=j+1
  end do

  if(mpirank/=0)  then
    call mpi_send(j,1,mpi_integer8,0,1,mpi_comm_world,status,mpierr)
  else
    do k=1,mpisize-1
      call mpi_recv(i,1,mpi_integer8,k,1,mpi_comm_world,status,mpierr)
      j=j+i
    end do
!   write(*,'(a,i10)') 'Number of features after the redundant check is: ',j
  end if

END IF

end subroutine


subroutine iter_sis_p(neach,available,ftag,fname,feat,subs_sis,nout_sis,sisfeat,name_sisfeat)
! input: neach,available,ftag,fname
! output: nout_sis,sisfeat,name_sisfeat
! undefined variables come from the main program.

real*8 ftag(:),time_start,time_end,tmp,intercept,sisfeat(:,:),res(sum(nsample),ntask_nc),&
pscore(mpisize,2),pftag(mpisize),feat(:,:)
real*8,allocatable:: beta(:),score(:,:)
integer*8 i,j,k,l,ll,mm1,mm2,mpii,mpij,neach(:),nout_sis,loc(2),subs_sis
character(len=*) fname(:),name_sisfeat(:)
logical available(:),pavailable(mpisize)
integer mpierr,mpirank,mpisize,status(MPI_STATUS_SIZE)
common mpierr,mpirank,mpisize

allocate(beta(niter_sis))
allocate(score(neach(mpirank+1),2))

if(mpirank==0) then
   if(niter_sis<subs_sis) then
   write(*,'(a)') 'Final inner iterative sure independent screening ...'
   else 
   write(*,'(a)') 'Final sure independent screening ...'
   end if
end if

pavailable=.false.
if(neach(mpirank+1)>0) call update_availability(available,pavailable)
do l=1,mpisize
 call mpi_bcast(pavailable(l),1,mpi_logical,l-1,mpi_comm_world,mpierr)
end do

time_start=mpi_wtime()
if(neach(mpirank+1)>0) score=-1   ! initial score

i=0
res=trainy
! inner iterative sure independent screening loop
do while(i<subs_sis .and. any(pavailable))

    ! time recording
    if(i==niter_sis .and. mpirank==0) then
       time_end=mpi_wtime()
       write(*,'(a,f15.2)') 'Estimated time remaining for the screening (s): ',&
                 (time_end-time_start)*(subs_sis-niter_sis)/float(niter_sis)
    end if

    ! get the residuals
     IF(mpirank==0 ) THEN
        if(i>=niter_sis) then
           do l=1,ntask_nc
           do ll=1,ntask_1c
             mm1=sum(nsample(:ll-1))+1
             mm2=sum(nsample(:ll))
             j=0
             if(nsample(ll)<niter_sis) j=niter_sis-nsample(ll) ! to avoid ill defined system when (tsize(ll)<mcycle)
             call orth_de(sisfeat(mm1:mm2,i-niter_sis+1:i-j),trainy(mm1:mm2,l),intercept,beta(:niter_sis-j),tmp)

             k=0
             do while(tmp>SD(ll,l) .or. isnan(tmp) )
                 k=k+1
                 call orth_de(sisfeat(mm1:mm2,i-niter_sis+1:i-j-k),trainy(mm1:mm2,l),intercept,beta(:niter_sis-j-k),tmp)
             end do
          res(mm1:mm2,l)=trainy(mm1:mm2,l)-(intercept+matmul(sisfeat(mm1:mm2,i-niter_sis+1:i-j-k),beta(:niter_sis-j-k)))
           end do
           end do
        end if
     END IF

     ! broadcast the residuals
      if(i>=niter_sis) call mpi_bcast(res,sum(nsample)*ntask_nc,mpi_double_precision,0,mpi_comm_world,mpierr)

     ! get the scoring quantity
     if(trim(adjustl(ptype))=='quanti') then
        do k=1,neach(mpirank+1)
          if(available(k)) score(k,:)=sis_score(feat(:,k),res,ntask_nc,ntask_1c,'not_centered')
        end do
     else if(trim(adjustl(ptype))=='quali') then
        do k=1,neach(mpirank+1)
          if(available(k)) score(k,:)=sis_score(feat(:,k),res,ntask_nc,ntask_1c,'centered') ! for classification
        end do
     end if


    ! selection starts ...
    k=0
    do while( k<niter_sis .and. any(pavailable) .and. i<subs_sis)
         k=k+1
         i=i+1

          ! find the max score with the max ftag on each core
          if(neach(mpirank+1)>0) then
            loc(2:2)=maxloc(score(:,1))
            tmp=score(loc(2),1)
          end if
          do l=1,neach(mpirank+1)
            if(  abs(tmp-score(l,1))<1d-10 ) then
               if( score(l,2)>score(loc(2),2) .or. &
                    (abs(score(l,2)-score(loc(2),2))<1d-10 .and.  ftag(l)>ftag(loc(2))) ) loc(2)=l
            end if
          end do
          if(neach(mpirank+1)>0) then
            pscore(mpirank+1,:)=score(loc(2),:)
            pftag(mpirank+1)=ftag(loc(2))
          else
            pscore(mpirank+1,:)=-1   ! smaller than score of any available features
            pftag(mpirank+1)=-1
          end if

          !broadcast to let all the core has the same data
          do l=1,mpisize
            call mpi_bcast(pscore(l,:),2,mpi_double_precision,l-1,mpi_comm_world,mpierr)
            call mpi_bcast(pftag(l),1,mpi_double_precision,l-1,mpi_comm_world,mpierr)
          end do

          !find the max score among all the cores
          loc(1:1)=maxloc(pscore(:,1))
          tmp=maxval(pscore(:,1))
          do l=1,mpisize
            if(  abs(tmp-pscore(l,1))<1d-10 ) then
               if( pscore(l,2)>pscore(loc(1),2) .or. &
                    ( abs(pscore(l,2)-pscore(loc(1),2))<1d-10 .and.  pftag(l)>pftag(loc(1))) ) loc(1)=l
            end if
          end do

          ! store the important features
          if((loc(1)-1)==mpirank) then
             available(loc(2))=.false.
             score(loc(2),:)=-1
             if(mpirank==0) then
                 sisfeat(:,i)=feat(:,loc(2))
                 name_sisfeat(i)=fname(loc(2))
             else
                call mpi_send(feat(:,loc(2)),sum(nsample),mpi_double_precision,0,100,mpi_comm_world,status,mpierr)
                call mpi_send(fname(loc(2)),lname,mpi_character,0,101,mpi_comm_world,status,mpierr)
             end if
          end if

          if(mpirank==0 .and. mpirank/=(loc(1)-1) ) then
               call mpi_recv(sisfeat(:,i),sum(nsample),mpi_double_precision,loc(1)-1,100,mpi_comm_world,status,mpierr)
               call mpi_recv(name_sisfeat(i),lname,mpi_character,loc(1)-1,101,mpi_comm_world,status,mpierr)
          end if
          !---

          call mpi_barrier(mpi_comm_world,mpierr)

          if(neach(mpirank+1)>0) call update_availability(available,pavailable)
          do l=1,mpisize
           call mpi_bcast(pavailable(l),1,mpi_logical,l-1,mpi_comm_world,mpierr)
          end do
     end do
     !--------------------

end do  ! end inner iterative sure independent screening loop
nout_sis=i    ! the actual number of selected features
deallocate(score)
deallocate(beta)
end subroutine


subroutine dup_scheck(nf,ftag,fname,order,available)
! output order and available
integer*8 i,j,l,ll,order(:),n,nf
real*8 ftag(:)
character(len=*) fname(:)
logical available(:)
integer mpierr,mpirank,mpisize,status(MPI_STATUS_SIZE)
common mpierr,mpirank,mpisize

!if(mpirank==0) write(*,'(a)') 'Redundant check on each core starts ...'
IF(nf==0) THEN
  n=0

ELSE

  ! ordering from large to small
  order(1)=1
  n=1
  do i=2,nf

    l=0; ll=n;
    j=l+ceiling(float(ll-l)/2.0)

    123 continue
    if(abs(ftag(i)-ftag(order(j)))<=1d-10 ) then  ! if equal
         if(better_name(fname(i),fname(order(j)))==2) then
            available(i)=.false.
         else
            available(order(j))=.false.
            order(j)=i
         end if
         cycle
    else   ! if not equal
      if(ftag(i)>ftag(order(j))) then
         ll=j
         if(j==l+ceiling(float(ll-l)/2.0)) then
           order(j+1:n+1)=order(j:n)
           order(j)=i
           n=n+1
           cycle
         end if
       else
          l=j
          if(j==l+ceiling(float(ll-l)/2.0)) then
            if(n>j) order(j+2:n+1)=order(j+1:n)
            order(j+1)=i
            n=n+1
            cycle
          end if
       end if
       j=l+ceiling(float(ll-l)/2.0)
       goto 123
     end if
   end do

END IF

order(nf+1)=n  ! store the number of features after check

if(mpirank/=0)  then
  call mpi_send(n,1,mpi_integer8,0,1,mpi_comm_world,status,mpierr)
else
  do l=1,mpisize-1
    call mpi_recv(i,1,mpi_integer8,l,1,mpi_comm_world,status,mpierr)
    n=n+i
 end do
!   write(*,'(a,i10)') 'Number of features after the redundant check is: ',n
end if

end subroutine

function sis_score(feat,y,ntask_nc,ntask_1c,center)   
! high scored features will be selected
! sis_score is a 2-vector function, with the 1st the main metric and the other the 2nd metric
integer i,j,mm1,mm2,mm3,mm4,k,kk,ntask_nc,ntask_1c,l,overlap_n,nf1,nf2,itask
character(len=*) center
real*8 feat(:),sdfeat(ubound(feat,1)),tmp(ntask_1c,ntask_nc),sis_score(2),y(:,:),&
sdy(ubound(y,1),ubound(y,2)),xnorm(ntask_1c),xmean(ntask_1c),intercept,beta(1),overlap_length,length_tmp,&
feat_tmp1(ubound(feat,1)),feat_tmp2(ubound(feat,1)),mindist,minlen
logical isoverlap

if(trim(adjustl(center))=='centered') then
   sdy=y
else
   do i=1,ntask_nc
   do j=1,ntask_1c
   mm1=sum(nsample(:j-1))+1
   mm2=sum(nsample(:j))
   sdy(mm1:mm2,i)=y(mm1:mm2,i)-sum(y(mm1:mm2,i))/(mm2-mm1+1)
   end do
   end do
end if


if(trim(adjustl(ptype))=='quanti') then

   do j=1,ntask_1c
       mm1=sum(nsample(:j-1))+1
       mm2=sum(nsample(:j))
       xmean(j)=sum(feat(mm1:mm2))/nsample(j)
       sdfeat(mm1:mm2)=feat(mm1:mm2)-xmean(j)  
       xnorm(j)=sqrt(sum((sdfeat(mm1:mm2))**2))
       if(xnorm(j)>1d-50) sdfeat(mm1:mm2)=sdfeat(mm1:mm2)/xnorm(j)
       do i=1,ntask_nc
          tmp(j,i)=abs(sum(sdfeat(mm1:mm2)*sdy(mm1:mm2,i)))   ! |xy| absolute of correlation of this task
       end do                                                 ! sdy not normalized means each y has the same weight.
   end do
  ! score ranges from 0 to 1
  sis_score(1)=sqrt(sum(tmp**2)/(ntask_nc*ntask_1c))  ! quadratic mean over tasks
  sis_score(1)=sis_score(1)/sqrt(sum(sdy**2)/(ntask_nc*ntask_1c))  ! normalize over task averaged norm
  sis_score(2)=1.d0   ! useless

else if(trim(adjustl(ptype))=='quali') then  
  
   mindist=-1d10
   overlap_n=0
   overlap_length=0.d0
   isoverlap=.false.

   do itask=1,ntask_1c
   ! calculate overlap between domains of task i
        do i=1,ngroup(itask,1000)-1   ! ngroup(itask,1000) record the number of groups in this task
            if(itask==1) then
              mm1=sum(ngroup(itask,:i-1))+1
              mm2=sum(ngroup(itask,:i))
            else            
              mm1=sum(nsample(:itask-1))+sum(ngroup(itask,:i-1))+1
              mm2=sum(nsample(:itask-1))+sum(ngroup(itask,:i))
            end if
            nf1=0
            do k=mm1,mm2
               if(sdy(k,1)<0.5) then  ! y=1 denotes classified, y=0 denotes unclassified
                   nf1=nf1+1
                   feat_tmp1(nf1)=feat(k) 
               end if
            end do       
            if(nf1==0) cycle
          do j=i+1,ngroup(itask,1000) 
               if(itask==1) then
                 mm3=sum(ngroup(itask,:j-1))+1
                 mm4=sum(ngroup(itask,:j))
               else
                 mm3=sum(nsample(:itask-1))+sum(ngroup(itask,:j-1))+1
                 mm4=sum(nsample(:itask-1))+sum(ngroup(itask,:j))
               end if
               nf2=0
               do k=mm3,mm4
                   if(sdy(k,1)<0.5) then
                       nf2=nf2+1
                       feat_tmp2(nf2)=feat(k) 
                   end if
               end do
               if(nf2==0 .or. (nf1==1 .and. nf2==1)  ) cycle

               call convex1d_overlap(feat_tmp1(:nf1),feat_tmp2(:nf2),width,k,length_tmp)
               overlap_n=overlap_n+k
               if(length_tmp>=0.d0) isoverlap=.true.

            ! which feature is shorter
            minlen=min(maxval(feat_tmp1(:nf1))-minval(feat_tmp1(:nf1)),maxval(feat_tmp2(:nf2))-minval(feat_tmp2(:nf2)))

               if(length_tmp<0.d0) then  ! if separated
                  if(length_tmp>mindist) mindist=length_tmp  ! renew the worst separation
               else if(length_tmp>=0.d0 .and. minlen==0.d0) then  ! if overlapped and one feature is 0D
                  overlap_length=overlap_length+1.d0  ! totally overlapped
               else if(length_tmp>=0.d0 .and. minlen>0.d0) then  ! if separated and no 0D feature
                  overlap_length=overlap_length+length_tmp/minlen  ! calculate total overlap
               end if

          end do  ! j
        end do  ! i
   end do  ! itask

   sis_score(1)=float(overlap_n)  ! >=0,larger sis_score,worse feature
   sis_score(1)=1.d0/(sis_score(1)+1.d0)  ! transform to <=1, larger sis_score,better feature
   ! total pairs
   j=0
   do i=1,ntask_1c   
     j=j+ngroup(i,1000)*(ngroup(i,1000)-1)/2
   end do

   if(isoverlap) then  ! there are domains overlapped
     sis_score(2)=overlap_length/float(j)     ! second metric, <=1, larger, worse
     sis_score(2)=1.d0/(sis_score(2)+1.0)  ! transform to <=1, larger, better
   else ! separated length
     sis_score(2)=-mindist   ! separated length between domains,positive
   end if

end if

end function


subroutine isgoodf(feat,name_feat,lastop,dimens,nf,foutsave)
real*8 feat(:),dimens(:)
character(len=*) name_feat,lastop
integer*8 nf,nomb
logical foutsave

nomb = 50000
if(goodf(feat,name_feat,dimens)) then
  nf=nf+1
  if(foutsave) then
  if(nf>ubound(fout,2)) call addm_out(nomb)
  fout(:,nf)=feat
  name_fout(nf)=name_feat
  lastop_out(nf)=lastop
  dimout(:,nf)=dimens
  end if
end if
end subroutine

subroutine isgoodvf(feat,name_feat,lastop,dimens,nf,nvf,foutsave)
real*8 feat(:,:),dimens(:),feat2(ubound(feat,1))
character(len=*) name_feat,lastop
integer*8 nf,nvf,d1,i,nomb,nomb2
logical foutsave

nomb=50000
nomb2=5000
d1=ubound(feat,1)

! transform
do i=1,d1
  if(trim(vf2sf)=='sum') then
     feat2(i)=sum(feat(i,:))  ! sum of elements
  else if (trim(vf2sf)=='norm') then
     feat2(i)=sqrt(sum((feat(i,:))**2))
  else if (trim(vf2sf)=='min') then
     feat2(i)=minval(feat(i,:))
  else if(trim(vf2sf)=='max') then
     feat2(i)=maxval(feat(i,:))
  end if
end do
   

if(goodf(feat2,name_feat,dimens)) then
  nf=nf+1
  nvf=nvf+1
  if(foutsave) then
  if(nf>ubound(fout,2)) call addm_out(nomb)
  if(nvf>ubound(vfeat,3)) call addm_vf(nomb2)
  fout(1,nf)=nvf
  fout(2,nf)=abs(sum(tag*feat2))/ntask_1c
  vfeat(:,:,nvf)=feat
  name_fout(nf)=name_feat
  lastop_out(nf)=lastop
  dimout(:,nf)=dimens
  end if
end if
end subroutine

function isscalar(fname)
logical isscalar
character(len=*) fname
isscalar=.false.  ! initialization
if(nvf_init==0) then
isscalar=.true.
return
else if(index(fname,'v_')==0) then
isscalar=.true.
end if
end function

!function isreal(fname)
!logical isreal
!character(len=*) fname
!isreal=.false.  !initialization
!if(nif==0) then
!isreal=.true.
!return
!else if(index(fname,'vr_')/=0 .or. index(fname,'sr_')/=0) then
!isreal=.true.
!end if
!end function

function isABC(fname)
logical isABC
character(len=*) fname
isABC=.false.  ! initialization

if(nele==6) then
   if ( index(fname,'A')/=0 .and. index(fname,'B')/=0 .and. index(fname,'C')/=0 .and. &
   index(fname,'D')/=0 .and. index(fname,'E')/=0 .and. index(fname,'F')/=0 ) isABC=.true.
else if(nele==5) then
   if ( index(fname,'A')/=0 .and. index(fname,'B')/=0 .and. index(fname,'C')/=0 .and. &
   index(fname,'D')/=0 .and. index(fname,'E')/=0 ) isABC=.true.
else if(nele==4) then
   if ( index(fname,'A')/=0 .and. index(fname,'B')/=0 .and. index(fname,'C')/=0 .and. &
   index(fname,'D')/=0 ) isABC=.true.
else if(nele==3) then
   if ( index(fname,'A')/=0 .and. index(fname,'B')/=0 .and. index(fname,'C')/=0 ) isABC=.true.
else if(nele==2) then
   if ( index(fname,'A')/=0 .and. index(fname,'B')/=0 ) isABC=.true.
end if

end function

subroutine sis_s(subs_sis,nselect,fselected,name_fselected,ftag,score)
! delete redundant feature, reduce size to subs_sis
integer*8 i,j,k,l,ll,nselect,order(nselect),n,subs_sis
real*8 ftag(:),score(:,:),fselected(:,:),tmpf(ubound(fselected,1),subs_sis),tmpftag(subs_sis),tmpscore(subs_sis,2)
character(len=lname) name_fselected(:),tmpname(subs_sis)

! ordering from large to small
order(1)=1
n=1
do i=2,nselect

  l=0; ll=n;
  j=l+ceiling(float(ll-l)/2.0)
 
  125 continue
    if(abs(score(i,1)-score(order(j),1))<=1d-10 .and. abs(ftag(i)-ftag(order(j)))<=1d-10  ) then  ! if equal
         if(better_name(name_fselected(i),name_fselected(order(j)))==1) order(j)=i
         cycle
    else   ! if not equal, select those with large score
      if( (score(i,1)-score(order(j),1))>1d-10 .or. &
          (abs(score(i,1)-score(order(j),1))<=1d-10 .and. score(i,2)-score(order(j),2)>1d-10 ) .or. &
          (abs(score(i,1)-score(order(j),1))<=1d-10 .and. abs(score(i,2)-score(order(j),2))<=1d-10  &
                                                             .and.(ftag(i)-ftag(order(j)))>1d-10) ) then
         ll=j
         if(j==l+ceiling(float(ll-l)/2.0)) then
           order(j+1:n+1)=order(j:n)
           order(j)=i
           n=n+1
           cycle
         end if
       else
          l=j
          if(j==l+ceiling(float(ll-l)/2.0)) then
            if(n>j) order(j+2:n+1)=order(j+1:n)
            order(j+1)=i
            n=n+1
            cycle
          end if
       end if
       j=l+ceiling(float(ll-l)/2.0)
       goto 125
    end if
end do

n=min(n,subs_sis)
do i=1,n
tmpf(:,i)=fselected(:,order(i))
tmpname(i)=name_fselected(order(i))
tmpftag(i)=ftag(order(i))
tmpscore(i,:)=score(order(i),:)
end do

nselect=n
fselected(:,:n)=tmpf(:,:n)
name_fselected(:subs_sis)=tmpname(:n)
ftag(:n)=tmpftag(:n)
score(:n,:)=tmpscore(:n,:)

end subroutine


end program

