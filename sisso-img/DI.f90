program descriptor_identification

!----------------------------------------------------------------------------------
!Copyright 2017 Runhai Ouyang
!
!   Licensed under the Apache License, Version 2.0 (the "License");
!   you may not use this file except in compliance with the License.
!   You may obtain a copy of the License at
!
!       http://www.apache.org/licenses/LICENSE-2.0
!
!   Unless required by applicable law or agreed to in writing, software
!   distributed under the License is distributed on an "AS IS" BASIS,
!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
!   See the License for the specific language governing permissions and
!   limitations under the License.
!----------------------------------------------------------------------------------

!****************************************************************************************************************
! Descriptor identification by L0 or LASSO-L0 !
! Version FCDI.1.0, April, 2017
! *************************************************************************************************************** 

use libms

implicit none
include 'mpif.h'
real*8,allocatable:: x(:,:,:),y(:,:),beta(:,:),beta_init(:,:),coeff(:,:),xprime(:,:,:),xdprime(:,:,:),xmean(:,:),&
norm_xprime(:,:),yprime(:,:),prod_xty(:,:),prod_xtx(:,:,:),SD(:),lassormse(:),ymean(:),intercept(:),weight(:,:)
real*8 lambda,lambda_max,elastic,tole,alpha,minL1rmse,totalrmse,totalSD,width
integer max_iter,run_iter,i,j,k,l,ll,i1,i2,i3,i4,i5,i6,nf,nfL0,maxns,size_fs,ntry,nlambda,nactive,dens,ntask,&
desc_dim,CV_fold,CV_repeat,n_eval,n_out,iFCDI
integer,allocatable:: idf(:),activeset(:),ncol(:),nsample(:),ngroup(:,:)
character line_para*500,line*500,taskname*20,method*10,metric*10,ptype*10,nsample_line*500
character,allocatable:: fname(:)*200
logical warm_start,isnew,weighted,lFCDI
! mpi
real*8 stime,etime
integer mpierr,mpirank,mpisize,status(MPI_STATUS_SIZE),mpii,mpij,mpin
common mpierr,mpirank,mpisize

!------------------------------
! MPI starts
!------------------------------
call mpi_init(mpierr)
call mpi_comm_size(mpi_comm_world,mpisize,mpierr)
call mpi_comm_rank(mpi_comm_world,mpirank,mpierr)
if(mpirank==0) stime=mpi_wtime()


call random_seed()
!--

!---------------------
! initialization
!---------------------

ntask=1                      ! number of task (number of taskxxx.dat)
size_fs=1                    ! size of the feature space for descriptor identification
nfL0= 1                      ! number of features for L0(size_fs->nfL0 if nfL0>size_fs)
method='L0'                  ! 'L1L0' or 'L0'

max_iter=1e6                 ! max iteration for LASSO of each lambda to stop
tole=1e-6                    ! convergence criteria for LASSO of each lambda to stop
dens=120                     ! density of lambda grid = number of points in [0.001*max,max]
nlambda=1e3                  ! max number of lambda points
minL1rmse=1e-3               ! Min RMSE for the L1 to stop
warm_start=.true.            ! use previous solution for the next step
weighted=.false.             ! weighted observations? (provide file task.weight if yes)

ptype='quanti'               ! property type: 'quanti'(quantitative),'quali'(qualitative)
desc_dim=1                     ! descriptors up to desc_dim dimension will be calculated
n_out=100                    ! number of top models to be output, off when =0

metric='LS_RMSE'             ! metric for the evaluation: LS_RMSE,CV_RMSE,CV_MAE
n_eval=1000                  ! number of top models (based on fitting) to be evaluated by the metric
CV_fold=10                   ! k-fold CV for each model: >=2
CV_repeat=1                  ! repeated k-fold CV

width=0.d0                   ! the classification tolerance at domain boundaries

!------------------------------------
!parameters read in from FCDI.in
!------------------------------------
open(100,file='FCDI.in',status='old')
100 read(100,'(a)',end=101) line_para
if(index(line_para,'!')/=0) line_para(index(line_para,'!'):)=''
i=index(line_para,'=')
if(i>0) then
select case (trim(adjustl(line_para(1:i-1))))
case('ntask')
read(line_para(i+1:),*) ntask
case('nsample') ! number of samples  for each task
read(line_para(i+1:),'(a)') nsample_line
case('size_fs')
read(line_para(i+1:),*) size_fs
case('nfL0')
read(line_para(i+1:),*) nfL0
case('method')
read(line_para(i+1:),*) method

case('max_iter')
read(line_para(i+1:),*) max_iter
case('tole')
read(line_para(i+1:),*) tole
case('nlambda')
read(line_para(i+1:),*) nlambda
case('dens')
read(line_para(i+1:),*) dens
case('minL1rmse')
read(line_para(i+1:),*) minL1rmse
case('warm_start')
read(line_para(i+1:),*) warm_start
case('weighted')
read(line_para(i+1:),*) weighted
case('elastic')
read(line_para(i+1:),*) elastic

case('ptype')
read(line_para(i+1:),*) ptype
case('desc_dim')
read(line_para(i+1:),*) desc_dim
case('n_out')
read(line_para(i+1:),*) n_out

case('metric')
read(line_para(i+1:),*) metric
case('CV_fold')
read(line_para(i+1:),*) CV_fold
case('CV_repeat')
read(line_para(i+1:),*) CV_repeat
case('n_eval')
read(line_para(i+1:),*) n_eval

case('width')
read(line_para(i+1:),*) width

end select
end if
goto 100
101 continue 
close(100)
!------------

allocate(nsample(ntask))  ! total samples for each task
allocate(ngroup(ntask,1000))  ! number of samples in each of the groups for each task
nsample=0
ngroup=0
if(trim(adjustl(ptype))=='quanti') then
  read(nsample_line,*) nsample
else
  do ll=1,ntask
    i=index(nsample_line,'(')
    j=index(nsample_line,')')
    l=0
    do k=i,j
       if(nsample_line(k:k)==',') l=l+1
    end do
    read(nsample_line(i+1:j-1),*) ngroup(ll,1:l+1)
    ngroup(ll,1000)=l+1
    nsample(ll)=sum(ngroup(ll,1:l+1))
    nsample_line(:j)=''
  end do
end if

maxns=maxval(nsample)
!--------------------------
allocate(x(maxns,size_fs,ntask))
allocate(y(maxns,ntask))
allocate(SD(ntask))
allocate(fname(size_fs))
allocate(weight(maxns,ntask))
allocate(activeset(nfL0))
!------------------------

inquire(file='iFCDI',exist=lFCDI)
if(lFCDI) then
  open(1,file='iFCDI',status='old')
  read(1,*) iFCDI
  close(1)
  desc_dim=iFCDI
end if


if(mpirank==0) then
  open(1,file='DI.out',status='replace')
  write(1,'(a)') 'Version FCDI.1.0, April, 2017'
  write(1,'(/a)') 'Parameters read in from FCDI.in:'
  write(1,'(a)') '---------------------------------------------------------------------------------'
  write(1,'(a,i8)') 'Number of task (number of taskxxx.dat): ',ntask
1001 format(a,*(i8))
  write(1,1001) 'Number of data points in each task: ',nsample
  write(1,'(a,i8)') 'Total number of features in the feature space: ',size_fs
  write(1,'(a,a)') 'Method for descriptor identification  ',method
  write(1,'(a,i8)') 'Number of features for L0: ',nfL0
  
  if(trim(adjustl(method))=='L1L0') then
  write(1,'(a,i10)') 'Max iterations for LASSO (with given lambda) to stop: ',max_iter
  write(1,'(a,e15.5)') 'Convergence criteria for LASSO: ',tole
  write(1,'(a,i8)') 'Number of lambda trial: ',nlambda
  write(1,'(a,i8)') 'Density of lambda points: ',dens
  write(1,'(a,e15.5)') 'Minimal RMSE for LASSO to stop: ',minL1rmse
  write(1,'(a,l6)') 'Weighted observations (if yes, provide file task.weight)? ',weighted
  write(1,'(a,l6)') 'Warm start?  ',warm_start
  end if
  
  !write(1,'(a,e15.5)') 'elastic net: ',elastic
  
  write(1,'(a,a)') 'Property type: "quanti"(quantitative),"quali"(qualitative): ',trim(ptype)
  if(trim(adjustl(ptype))=='quali') then
1002 format(a,i3,a,*(i5))
    do i=1,ntask
       write(1,1002) 'Number of samples in each group of task ',i,': ',ngroup(i,:ngroup(i,1000))
    end do
    write(1,'(a,f10.6)') 'Classification tolerance at domain boundaries: ',width
  end if
  
  
  write(1,'(a,i8)') 'Max descriptor dimension for this L0: ',desc_dim
  write(1,'(a,i8)') 'Number of best models to be output: ',n_out
  if(trim(adjustl(ptype))=='quanti') then
    write(1,'(a,a)')  'Metric for L0: ',trim(metric)
    write(1,'(a,i8)') 'Number of best models (based on fitting) to be  evaluated with the metric: ',n_eval
    write(1,'(a,i8)') 'Fold number of the k-fold inner CV? ',CV_fold
    write(1,'(a,i8)') 'Number of repeat for the inner CV: ',CV_repeat
  end if
  
  write(1,'(a)') '---------------------------------------------------------------------------------'
end if

!------------------------
! job assignment for CPU
!------------------------
allocate(ncol(mpisize))
mpii=size_fs/mpisize
mpij=mod(size_fs,mpisize)
ncol(1)=mpii
do mpin=1,mpisize-1
if(mpin<=mpij) then
ncol(1+mpin)=mpii+1
else
ncol(1+mpin)=mpii
end if
end do

!-----------------------
! data read in
!-----------------------
y=0
x=0
do i=1,ntask
  write(taskname,'(a,i3.3,a)') 'task',i,'.dat'
  open(2,file=trim(adjustl(taskname)),status='old')
   if(trim(adjustl(ptype))=='quanti') then
      do j=1,nsample(i)
         read(2,*) y(j,i),x(j,:,i)
      end do
   else
      do j=1,nsample(i)
         read(2,*) x(j,:,i)
         y(j,i)=0.d0  
      end do
   end if
   close(2)
end do


open(3,file='task.fname',status='old')
do i=1,size_fs
read(3,'(a)') line
line=trim(adjustl(line))
fname(i)=line(1:index(line,' '))
end do
close(3)

weight=1.0
if(weighted) then
open(3,file='task.weight',status='old')
read(3,*) ! title line
do i=1,maxns
   read(3,*) weight(i,:)
end do
close(3)
end if

!---------------------
! SD of the properties
!---------------------
do i=1,ntask
SD(i)=sqrt(sum((y(:nsample(i),i)-sum(y(:nsample(i),i))/nsample(i))**2)/nsample(i)) !standard deviation
end do
totalSD=sqrt(sum(SD**2*nsample)/sum(nsample)) ! overall SD
2003 format(a,*(f10.6))
if(mpirank==0 .and. trim(adjustl(ptype))=='quanti') then
write(1,2003) 'SD of the tasks: ',SD
write(1,'(a,f10.6)') 'The overall SD is: ',totalSD
end if

!----------
if(nfL0>size_fs) nfL0=size_fs


!-----------------------------------------------------
! MODEL SELECTION (L0 or L1L0)
!-----------------------------------------------------

if(trim(adjustl(method))=='L1L0' .and. size_fs/=nfL0 .and. trim(adjustl(ptype))=='quanti') then
    !----------------
    ! L1 of the L1L0
    !----------------
    allocate(xprime(maxns,size_fs,ntask))
    allocate(xdprime(maxns,size_fs,ntask))
    allocate(xmean(size_fs,ntask))
    allocate(norm_xprime(size_fs,ntask))
    allocate(prod_xtx(size_fs,ncol(mpirank+1),ntask))
    allocate(intercept(ntask))
    allocate(beta(size_fs,ntask)) 
    allocate(beta_init(size_fs,ntask))
    allocate(yprime(maxns,ntask))
    allocate(prod_xty(size_fs,ntask))
    allocate(lassormse(ntask))
    allocate(ymean(ntask))
    
    ! initialization for L1
    xprime=0
    xdprime=0
    xmean=0
    norm_xprime=0
    prod_xtx=0
    beta=0
    beta_init=0.0
    yprime=0
    prod_xty=0
    lassormse=0
    ymean=0
    !---------
    
    !---------------------------------------------------------------------
    ! standardize and precompute the data for lasso
    ! transform y=a+xb into y=xb; y'=y-ymean; x'(:,j)=x(:,j)-xmean(j); 
    ! x(:,j)''=x(:,j)'/nomr(x'(j)); a=ymean-sum(xmean*b); b'=b*norm(x')
    ! after solving b', do b'->b and calculate a to get y=a+bx
    ! weight is applied to square error, not the training property.
    !---------------------------------------------------------------------
    if(mpirank==0) write(1,'(/a)') 'data standardizing ... '
    !yprime
    do i=1,ntask
    ymean(i)=sum(y(:nsample(i),i))/nsample(i)
    yprime(:nsample(i),i)=y(:nsample(i),i)-ymean(i)
    end do
    
    do i=1,ntask
       !xprime and xdprime
       do j=1,size_fs
         xmean(j,i)=sum(x(:nsample(i),j,i))/nsample(i)
         xprime(:nsample(i),j,i)=x(:nsample(i),j,i)-xmean(j,i)
         norm_xprime(j,i)=sqrt(sum(xprime(:nsample(i),j,i)**2))
         if(abs(norm_xprime(j,i))<1d-10) then
         if(mpirank==0) print *, 'Error: norm of feature ',j,' of task ',i,' is zero'
         stop
         end if
         xdprime(:nsample(i),j,i)=xprime(:nsample(i),j,i)/norm_xprime(j,i)
       end do
       !xty
       prod_xty(:,i)=matmul(transpose(xdprime(:nsample(i),:,i)),weight(:nsample(i),i)*yprime(:nsample(i),i))
       !xtx=(xtx)t !prod_xtx(mpii,:)=prod_xtx(:,mpii)
       do mpii=1,ncol(mpirank+1)
       prod_xtx(:,mpii,i)=matmul(xdprime(:nsample(i),mpii+sum(ncol(1:mpirank)),i)*weight(:nsample(i),i),&
                                          xdprime(:nsample(i),:,i))
       end do
    
    end do
    
    !--------------------------------------------------------------------------------------------------------------
    ! get lambda_max
    ! see J. Friedman, T. Hastie, and R. Tibshirani 2010,"Regularization Paths for Generalized Linear Models via CD"
    ! "J. Friedman, T. Hastie, and R. Tibshirani, 2010 "A note on the group lasso and a sparse group lasso"
    !---------------------------------------------------------------------------------------------------------------
    lambda_max=0
    do j=1,size_fs
    lambda=sqrt(sum(prod_xty(j,:)**2))
    if(lambda>lambda_max) lambda_max=lambda
    end do
    
    if(mpirank==0) then
    write(1,'(/a)') 'feature selection by multi-task lasso starts ... '
    end if
    
    !----------------------------------------
    ! (Elastic net) H. Zou, and T. Hastie, J. R. Statist. Soc. B 67, 301 (2005).
    ! elastic=1 is back to lasso (Eq. 14)
    !----------------------------------------
    !if(elastic<1.0) then
    !prod_xtx=elastic*prod_xtx
    !do j=1,size_fs
    !prod_xtx(j,j,:)=prod_xtx(j,j,:)+(1-elastic)
    !end do
    !end if
    
    !---------------------------------------------------------------------------
    ! Calculating lasso path with decreasing lambdas
    !---------------------------------------------------------------------------
    
    !counting the total size of the active set
    nactive=0
    ! iteration starts here
    do ntry=1,nlambda
        ! create lambda sequence
        ! max and min in log scale [log10(lambda_max),log10(0.001*lambda_max)]
        ! density, i.e.: 100 points, log-interval=3/100=0.03
        ! lambda_i=10^(log(lmax)-0.03*(i-1))
        lambda=10**(log10(lambda_max)-3.0/dens*(ntry-1))
    
        ! call mtlasso
        call mtlasso_mpi(prod_xty,prod_xtx,lambda,max_iter,tole,beta_init,run_iter,beta,nf,ncol)
        
        ! lasso rmse
        do i=1,ntask
         lassormse(i)=sqrt(sum(weight(:nsample(i),i)*(yprime(:nsample(i),i)-matmul(xdprime(:nsample(i),:,i),&
                       beta(:,i)))**2)/nsample(i))  ! RMSE
        end do
        totalrmse=sqrt(sum(lassormse**2*nsample)/sum(nsample))  ! overall RMSE
    
        ! warm_start
        if(warm_start) beta_init=beta
        
        ! intercept and beta
        do i=1,ntask
          intercept(i)=ymean(i)-sum(xmean(:,i)*beta(:,i)/norm_xprime(:,i))
        end do   
                       
        ! store the coeff. and id of features of this active set
        allocate(coeff(nf,ntask))
        allocate(idf(nf))
        coeff=0
        idf=0
        k=0
        do j=1,size_fs
          if(maxval(abs(beta(j,:)))>1d-10) then
           k=k+1
           idf(k)=j
           coeff(k,:)=beta(j,:)
          end if
        end do
        if(k/=nf .and. mpirank==0 ) stop 'ID of selected features not correctly identified !'
        
        ! add to the total active set
        isnew=.true.  
        do k=1,nf
          if(any(idf(k)==activeset(:nactive))) isnew=.false.
          if(isnew) then
             nactive=nactive+1
             activeset(nactive)=idf(k)
          end if
          isnew=.true.
          if(nactive==nfL0) exit
        end do
        
        ! output information of this lambda
        if(mpirank==0) then
        write(1,'(a/)') '--------------------------------------------------------------------------------------------'
        write(1,'(a,i5)') 'Try: ',ntry
        write(1,'(a)') '----------------'
        write(1,'(a30,e15.5)')  'Lambda: ', lambda
        write(1,'(a30,i10)')   'Number of iteration: ',run_iter
2004    format(a30,*(f10.6))
        write(1,2004)  ' Total LASSO RMSE: ', totalrmse
        write(1,2004)  ' LASSO RMSE for each property: ', lassormse
        write(1,'(a30,i6)')   'Size of this active set: ',nf
2005    format(a30,*(i8))
        if(nf/=0) write(1,2005)   'This active set: ',idf
        if(nf/=0) then
2006    format(a25,i3.3,a2,*(e15.5))
        do i=1,ntask
        write(1,2006) 'LASSO Coeff._',i,': ', coeff(:,i)
        end do
        end if
        write(1,'(a30,i6)') 'Size of the tot act. set: ',nactive
2007    format(a30,*(i8))
        if(nactive/=0) write(1,2007) 'Total active set: ',(activeset(j),j=1,nactive)
        end if
        deallocate(coeff)
        deallocate(idf)
        
        ! exit
        if(nactive>=nfL0) then
        if(mpirank==0) write(1,'(/a,i3)') 'Total number of selected features >= ',nfL0
        exit
        else if (nactive==size_fs) then
        if(mpirank==0) write(1,'(/a)') 'The whole feature space is already selected!'
        exit
        else if (ntry==nlambda) then
        if(mpirank==0) write(1,'(/a)') 'Number of lambda trials hits the max !'
        exit
        else if (maxval(lassormse)<minL1rmse) then
        if(mpirank==0) write(1,'(/a)') 'RMSE is already smaller than the criterion!'
        exit
        end if
    
    end do
    deallocate(prod_xty)
    deallocate(prod_xtx)
    deallocate(ncol)
    deallocate(beta)
    deallocate(beta_init)
    deallocate(xprime)
    deallocate(xdprime)
    deallocate(xmean)
    deallocate(yprime)
    deallocate(intercept)
    deallocate(norm_xprime)
    deallocate(ymean)
    deallocate(lassormse)
    if(mpirank==0) write(1,'(a)') '-------------- L1 finished ! -------------------------------------------'
    !---------------
    ! L0 of L1L0
    !---------------
     call model(x,y,fname,nactive,activeset)
 
else if (trim(adjustl(method))=='L0' .or. size_fs==nfL0) then
    !--------
    ! L0 only
    !--------
    nactive=nfL0
    do i=1,nfL0
       activeset(i)=i
    end do
    if(trim(adjustl(ptype))=='quanti') then
     call model(x,y,fname,nactive,activeset)
    else if (trim(adjustl(ptype))=='quali') then
     call model2(x,fname,nactive,activeset)
    end if
end if


! release space 
!--------------------
deallocate(x)
deallocate(y)
deallocate(SD)
deallocate(fname)
deallocate(activeset)
deallocate(nsample)
deallocate(ngroup)
deallocate(weight)

if(mpirank==0) then
etime=mpi_wtime()
write(1,'(/a)')   '----------------------------------------------------------------------------------'
write(1,'(a,f15.2)') 'Total time spent (second): ',etime-stime
write(1,'(/a/)') '                                               Have a nice day !    '
close(1)
end if
call mpi_finalize(mpierr)


contains

subroutine model(x,y,fname,nactive,activeset)
integer nactive,activeset(:),i,j,k,l,loc(1),ii(desc_dim),random(maxval(nsample),ntask,CV_repeat),ndim,&
select_model(max(n_eval,1),desc_dim),mID(max(n_eval,1),desc_dim)
integer*8 totalm,bigm,nall,nrecord
real*8  x(:,:,:),y(:,:),tmp,tmp2,yfit,CVrmse(ntask),CVmaxae(ntask),wrmse(ntask),LSrmse(ntask),LSmaxae(ntask),&
beta(desc_dim,ntask),intercept(ntask),select_LS(max(n_eval,1)),select_LSmaxae(max(n_eval,1)),&
select_CV(max(n_eval,1)),select_CVmaxae(max(n_eval,1)),mscore(max(n_eval,1),4),select_metric(max(n_eval,1))
character fname(:)*200,tname*12
integer mpierr,mpirank,mpisize,status(MPI_STATUS_SIZE),mpii,mpij
common mpierr,mpirank,mpisize
integer*8 njob(mpisize)
real*8 mpicollect(mpisize)
! model scores: RMSE,CV_RMSE,CV_max

if(n_eval<1) n_eval=1

!-------------
if(mpirank==0) then
write(1,'(/a,i10)') ' L0 starts with space size',nactive
write(1,'(a)')    '---------------------------------------------------------------------------------------'
end if
if(mpirank==0) call system('rm -rf desc_dat; mkdir desc_dat')

! generation of random order for repeated k-fold CV
do k=1,CV_repeat  
 do i=1,ntask
  call rand_order(random(:nsample(i),i,k))
 end do
end do


do ndim=1,desc_dim

   ! job assignment for each CPU
   nall=1
   do mpii=1,ndim
      nall=nall*(nactive-mpii+1)/mpii
   end do
   njob=nall/mpisize

   mpij=mod(nall,int8(mpisize))
   do mpii=1,mpisize-1
     if(mpii<=mpij) njob(mpii+1)=njob(mpii+1)+1   
   end do

   ! initialization   
   select_LS=10*maxval(abs(y))
   select_CV=10*maxval(abs(y))
   select_CVmaxae=10*maxval(abs(y))
   totalm=0

! get the best n_eval models on each CPU core
   nrecord=0
   do k=1,nactive
      ii(1)=k

      do j=2,ndim
        ii(j)=ii(j-1)+1   ! starting number
      end do      

      123 continue 
      nrecord=nrecord+1
      if( nrecord< sum(njob(:mpirank))+1 .or. nrecord> sum(njob(:mpirank+1)) ) goto 124

      !  RMSE of this model
      do i=1,ntask
       call orth_de(x(:nsample(i),[activeset(ii(:ndim))],i),y(:nsample(i),i),intercept(i),beta(:ndim,i),LSrmse(i))
       LSmaxae(i)= &
         maxval(abs(y(:nsample(i),i)-(intercept(i)+matmul(x(:nsample(i),[activeset(ii(:ndim))],i),beta(:ndim,i)))))
      end do
      tmp=sqrt(sum(LSrmse**2*nsample)/sum(nsample))  !overall error
      tmp2=sqrt(sum(LSmaxae**2)/ntask)         ! overall maxAE
      ! store good models with best RMSE
      if (any(tmp<select_LS)) then
         totalm=totalm+1
         loc=maxloc(select_LS)
         select_LS(loc(1))=tmp
         select_LSmaxae(loc(1))=tmp2
         select_model(loc(1),:ndim)=activeset(ii(:ndim))
      end if

      124 continue
      ! update the models(123,124,125,134,135,145,234,...)
      if(ndim==1) cycle
      ii(ndim)=ii(ndim)+1  ! add 1 for the highest dimension

      do j=ndim,3,-1
        if(ii(j)> (nactive-(ndim-j)) ) ii(j-1)=ii(j-1)+1 
      end do
      do j=3,ndim
        if(ii(j)> (nactive-ndim+j) ) ii(j)=ii(j-1)+1
      end do

      if(ii(2)>(nactive-(ndim-2)))  cycle

      goto 123
   end do

! get select_CV and select_CVmaxae of the good models
   do j=1,min(int8(n_eval),totalm)
       do i=1,ntask
          CVrmse(i)=0.0
          CVmaxae(i)=0.0
           do k=1,CV_repeat
            call foldCV(x(:nsample(i),[select_model(j,:ndim)],i),y(:nsample(i),i),random(:nsample(i),i,k),CV_fold,&
                        tmp,tmp2)
            CVrmse(i)=CVrmse(i)+tmp**2*nsample(i)  ! total sqaured error
            CVmaxae(i)=CVmaxae(i)+tmp2**2
           end do
       end do
       select_CV(j)=sqrt(sum(CVrmse)/CV_repeat/sum(nsample))  ! overall CV RMSE
       select_CVmaxae(j)=sqrt(sum(CVmaxae)/CV_repeat/ntask)
   end do

! collecting the best models over CPU cores
! select_model,select_LS,select_CV,select_CVmaxae,mID,mscore(RMSE,CV_RMSE,CV_max)
   if(mpirank>0) then
     call mpi_send(totalm,1,mpi_integer8,0,1,mpi_comm_world,status,mpierr)
   else
     do i=1,mpisize-1
       call mpi_recv(bigm,1,mpi_integer8,i,1,mpi_comm_world,status,mpierr)
       totalm=totalm+bigm
     end do
   end if
   call mpi_bcast(totalm,1,mpi_integer8,0,mpi_comm_world,mpierr)

   if(trim(adjustl(metric))=='LS_RMSE') then
     select_metric=select_LS
   else if (trim(adjustl(metric))=='CV_RMSE') then
     select_metric=select_CV
   else if (trim(adjustl(metric))=='CV_MAE') then
     select_metric=select_CVmaxae
   end if

   do j=1,min(int8(n_eval),totalm)
      loc=minloc(select_metric)
      k=loc(1)
      if(mpirank>0) then
        call mpi_send(select_metric(loc(1)),1,mpi_double_precision,0,1,mpi_comm_world,status,mpierr)
      else
        mpicollect(1)=select_metric(loc(1))
        do i=1,mpisize-1
          call mpi_recv(mpicollect(i+1),1,mpi_double_precision,i,1,mpi_comm_world,status,mpierr)
        end do
        loc=minloc(mpicollect)
      end if
      call mpi_bcast(loc(1),1,mpi_integer,0,mpi_comm_world,mpierr)

      if(mpirank==loc(1)-1) then
        mID(j,:ndim)=select_model(k,:ndim)
        mscore(j,:4)=(/select_LS(k),select_LSmaxae(k),select_CV(k),select_CVmaxae(k)/)
        select_metric(k)=10*maxval(abs(y))
      end if
      call mpi_bcast(mID(j,:ndim),ndim,mpi_integer,loc(1)-1,mpi_comm_world,mpierr)
      call mpi_bcast(mscore(j,:4),4,mpi_double_precision,loc(1)-1,mpi_comm_world,mpierr)
   end do
 

   if(mpirank==0) then
      ! LSRMSE, LSmaxae, CVRMSE,CVmaxae of the best model
      do i=1,ntask
        ! LS
        call orth_de(x(:nsample(i),[mID(1,:ndim)],i),y(:nsample(i),i),intercept(i),beta(:ndim,i),LSrmse(i))
         LSmaxae(i)= &
           maxval(abs(y(:nsample(i),i)-(intercept(i)+matmul(x(:nsample(i),[mID(1,:ndim)],i),beta(:ndim,i)))))
        ! CV
        CVrmse(i)=0.0
        CVmaxae(i)=0.0
        do k=1,CV_repeat
          call foldCV(x(:nsample(i),[mID(1,:ndim)],i),y(:nsample(i),i),random(:nsample(i),i,k),CV_fold,tmp,tmp2)
          CVrmse(i)=CVrmse(i)+tmp**2 ! sum of RMSE**2
          CVmaxae(i)=CVmaxae(i)+tmp2**2
        end do
      end do

      CVrmse=sqrt(CVrmse/CV_repeat) ! average over repeated CV
      CVmaxae=sqrt(CVmaxae/CV_repeat)   ! average over repeated CV
      call writeout(ntask,ndim,mID(1,:ndim),LSrmse,LSmaxae,beta,intercept,x,y,fname,nsample,CVrmse,CVmaxae)

      if(n_out>1) then
        write(tname,'(a,i4.4,a,i2.2,a)') 'top',min(int8(n_out),int8(n_eval),totalm),'_',ndim,'D'
        open(111,file=tname,status='replace')
        write(111,'(6a12)') 'Rank','LS_RMSE','LS_maxAE','CV_RMSE','CV_maxAE','Feature_ID'
2008    format(i12,4f12.6,a,*(i8))
        do i=1,min(int8(n_out),int8(n_eval),totalm)
          write(111,2008) i,mscore(i,:),'  |',mID(i,:ndim)
        end do
        close(111)
      end if
   end if

end do

call mpi_barrier(mpi_comm_world,mpierr)
end subroutine


subroutine writeout(ntask,ndim,id,LSrmse,LSmaxae,betamin,interceptmin,x,y,fname,nsample,CVrmse,CVmaxae)
integer ntask,ndim,nsample(:),i,j,id(:),k
real*8 LSrmse(:),LSmaxae(:),betamin(:,:),interceptmin(:),x(:,:,:),y(:,:),yfit,CVrmse(:),CVmaxae(:)
character fname(:)*200,descname*6,tname*3

descname(:4)='desc'
write(descname(5:5),'(i1.1)') ndim
descname(6:6)='_'

write(1,'(/i2,a)') ndim,'D Descriptor: '
write(1,'(a,4f10.6)')  'Total LS_RMSE,LS_maxAE, CV_RMSE,CV_maxAE: ',&
sqrt(sum(LSrmse**2*nsample)/sum(nsample)),sqrt(sum(LSmaxae**2)/ntask),sqrt(sum(CVrmse**2*nsample)/sum(nsample)),&
sqrt(sum(CVmaxae**2)/ntask)

2009 format(a25,*(i8,3a))
write(1,2009)  '@@@descriptor: ', (id(i),':[',trim(adjustl(fname(id(i)))),']',i=1,ndim)

2010 format(a20,a3,a2,*(e15.5))
2011 format(a10,2a15,*(a,i1.1))
2012 format(i10,*(e15.5))

do i=1,ntask
  write(tname,'(i3.3)') i
  write(1,2010) ' coefficients_',tname,': ', betamin(:ndim,i)
  write(1,'(a20,a3,a2,e15.5)') '  Intercept_',tname,': ', interceptmin(i)
  write(1,'(a20,a3,a2,2e15.5)') ' LSrmse,maxAE_',tname,': ', LSrmse(i),LSmaxae(i)
  write(1,'(a20,a3,a2,2e15.5)') ' CVrmse,maxAE_',tname,': ', CVrmse(i),CVmaxae(i)
  open(10,file='desc_dat/'//descname//tname//'.dat',status='replace')
  write(10,2011) 'Index','y_measurement','y_fitting',('   descriptor_',j,j=1,ndim)
  do j=1,nsample(i)
     yfit=interceptmin(i)+sum(x(j,[id(:ndim)],i)*betamin(:ndim,i))
     write(10,2012) j,y(j,i),yfit,x(j,[id(:ndim)],i)
  end do
  close(10)
end do

end subroutine


subroutine foldCV(x,y,random,CV_fold,CVrmse,CVmaxae)
! output CVrmse, CVmaxae
integer ns,fold,CV_fold,random(:),mm1,mm2,mm3,mm4,i,j,k,kk,l
real*8 x(:,:),y(:),beta(ubound(x,2)),intercept,rmse,CVse(CV_fold),CVrmse,CVmaxae,pred(ubound(y,1))

ns=ubound(y,1)
CVmaxae=0
CVrmse=0
fold=CV_fold
if(fold>ns) fold=ns

k=int(ns/fold)
kk=mod(ns,fold)
do l=1,fold
   mm1=1  ! sample start
   mm2=ns      ! sample end
   mm3=(l-1)*k+min((l-1),kk)+1 ! test start
   mm4=mm3+k-1+int(min(l,kk)/l)  ! test end
   if(mm1==mm3) then
      call orth_de(x([random(mm4+1:mm2)],:),y([random(mm4+1:mm2)]),intercept,beta(:),rmse)
   else if(mm4==mm2) then
      call orth_de(x([random(mm1:mm3-1)],:),y([random(mm1:mm3-1)]),intercept,beta(:),rmse)
   else
      call orth_de(x([random(mm1:mm3-1),random(mm4+1:mm2)],:),&
                   y([random(mm1:mm3-1),random(mm4+1:mm2)]),intercept,beta(:),rmse)
   end if
   pred(mm3:mm4)=(intercept+matmul(x([random(mm3:mm4)],:),beta(:)))
end do
CVmaxae=maxval(abs(y([random])-pred))
CVrmse=sqrt(sum((y([random])-pred)**2)/ns)  ! RMSE

end subroutine

subroutine rand_order(random)
! prepare random number for CV
integer random(:),i,j,k,ns
real tmp
ns=ubound(random,1)
random=0
   j=0
   do while(j<ns)
      call random_number(tmp)
      k=ceiling(tmp*ns)
      if(all(k/=random)) then
      j=j+1
      random(j)=k
      end if
   end do
end subroutine


! descriptor for classification
subroutine model2(x,fname,nactive,activeset)
integer nactive,activeset(:),i,j,k,l,loc(1),ii(desc_dim),itask,mm1,mm2,mm3,mm4,&
ndim,select_model(max(n_out,1),desc_dim),mID(max(n_out,1),desc_dim),overlap_n,overlap_n_tmp,&
select_overlap_n(max(n_out,1))
integer*8 totalm,bigm,nall,nrecord
real*8  x(:,:,:),mscore(max(n_out,1),2),overlap_area,overlap_area_tmp,select_overlap_area(max(n_out,1)),&
hull(maxval(nsample),2),area(maxval(ngroup(:,1000))),mindist,xtmp1(ubound(x,1),2),xtmp2(ubound(x,1),2)
character fname(:)*200,tname*12
integer mpierr,mpirank,mpisize,status(MPI_STATUS_SIZE),mpii,mpij
common mpierr,mpirank,mpisize
integer*8 njob(mpisize)
real*8 mpicollect(mpisize,2)
logical isoverlap
! model scores: RMSE,CV_RMSE,CV_max

if(n_out<1) n_out=1

!-------------
if(mpirank==0) then
write(1,'(/a,i10)')  ' L0 starts with space size',nactive
write(1,'(a)')   '---------------------------------------------------------------------------------'
end if
if(mpirank==0) call system('rm -rf desc_dat; mkdir desc_dat')


do ndim=1,desc_dim
  if(ndim>2) exit  ! up to 2D for now

   ! job assignment for each CPU core
   nall=1
   do mpii=1,ndim
      nall=nall*(nactive-mpii+1)/mpii
   end do
   njob=nall/mpisize

   mpij=mod(nall,int8(mpisize))
   do mpii=1,mpisize-1
     if(mpii<=mpij) njob(mpii+1)=njob(mpii+1)+1   
   end do

   ! initialization   
   select_overlap_n=sum(nsample)*sum(ngroup(:,1000))  ! set to a large value
   totalm=0

! get the best n_out models on each CPU core
   nrecord=0
   do k=1,nactive
      ii(1)=k

      do j=2,ndim
        ii(j)=ii(j-1)+1   ! starting number
      end do      

      123 continue 
      nrecord=nrecord+1
      if( nrecord< sum(njob(:mpirank))+1 .or. nrecord> sum(njob(:mpirank+1)) ) goto 124  ! job assignment

      ! initialization
      overlap_n=0
      overlap_area=0.d0
      mindist=-1d10
      isoverlap=.false.

      !------ get the score for this model ----------
      do itask=1,ntask
        ! area
           if(ndim==1) then
             do i=1,ngroup(itask,1000) ! number of groups in the task $itask
              mm1=sum(ngroup(itask,:i-1))+1 
              mm2=sum(ngroup(itask,:i))
              area(i)=maxval(x(mm1:mm2,[activeset(ii(:ndim))],itask))-minval(x(mm1:mm2,[activeset(ii(:ndim))],itask))
             end do
           else if(ndim==2) then
             do i=1,ngroup(itask,1000)
              mm1=sum(ngroup(itask,:i-1))+1 
              mm2=sum(ngroup(itask,:i))
              area(i)=convex2d_area(x(mm1:mm2,[activeset(ii(:ndim))],itask))
             end do
           end if

        !  model scores
           do i=1,ngroup(itask,1000)-1
             mm1=sum(ngroup(itask,:i-1))+1
             mm2=sum(ngroup(itask,:i))
             do j=i+1,ngroup(itask,1000)
               mm3=sum(ngroup(itask,:j-1))+1
               mm4=sum(ngroup(itask,:j))
               if(ndim==1) then
                   xtmp1(mm1:mm2,1)=x(mm1:mm2,activeset(ii(ndim)),itask)
                   xtmp2(mm3:mm4,1)=x(mm3:mm4,activeset(ii(ndim)),itask)
                   call convex1d_overlap(xtmp1(mm1:mm2,1),xtmp2(mm3:mm4,1),width,overlap_n_tmp,overlap_area_tmp)
               else if(ndim==2) then
                   xtmp1(mm1:mm2,:2)=x(mm1:mm2,activeset(ii(:2)),itask)
                   xtmp2(mm3:mm4,:2)=x(mm3:mm4,activeset(ii(:2)),itask)
                   call convex2d_overlap(xtmp1(mm1:mm2,:2),xtmp2(mm3:mm4,:2),width,overlap_n_tmp,overlap_area_tmp)
               end if

               overlap_n=overlap_n+overlap_n_tmp
               if(overlap_area_tmp>=0) isoverlap=.true.

               if(overlap_area_tmp<0.d0) then  ! if separated
                   if(mindist<overlap_area_tmp) mindist=overlap_area_tmp ! renew the worst separation
               else if(overlap_area_tmp>=0.d0 .and. min(area(i),area(j))==0.d0) then ! if overlapped with a 0D feature
                 overlap_area=overlap_area+1.d0   ! totally overlapped
               else if (overlap_area_tmp>=0.d0 .and. min(area(i),area(j))>0.d0) then ! if separated and no 0D feature
                 overlap_area=overlap_area+overlap_area_tmp/(min(area(i),area(j)))  ! calculate total overlap
               end if

             end do ! j
           end do ! i
         end do ! itask
         !----------------------------------

         j=0
         do i=1,ntask
           j=j+ngroup(i,1000)*(ngroup(i,1000)-1)/2
         end do

         if(isoverlap) then
            overlap_area=overlap_area/float(j) ! smaller, better
         else
            overlap_area=mindist  ! smaller, better
         end if


      ! store good models 
      if (any(overlap_n<select_overlap_n)) then
         totalm=totalm+1
         loc=maxloc(select_overlap_n)
         select_overlap_n(loc(1))=overlap_n
         select_overlap_area(loc(1))=overlap_area
         select_model(loc(1),:ndim)=activeset(ii(:ndim))
      else if (overlap_n==maxval(select_overlap_n) .and. any(overlap_area<select_overlap_area) )  then
         totalm=totalm+1
         loc=maxloc(select_overlap_area)
         select_overlap_n(loc(1))=overlap_n
         select_overlap_area(loc(1))=overlap_area
         select_model(loc(1),:ndim)=activeset(ii(:ndim))         
      end if

      ! update models(123,124,125,134,135,145,234,...)
      124 continue
      if(ndim==1) cycle
      ii(ndim)=ii(ndim)+1  ! add 1 for the highest dimension

      do j=ndim,3,-1
        if(ii(j)> (nactive-(ndim-j)) ) ii(j-1)=ii(j-1)+1 
      end do
      do j=3,ndim
        if(ii(j)> (nactive-ndim+j) ) ii(j)=ii(j-1)+1
      end do
      if(ii(2)>(nactive-(ndim-2)))  cycle
      goto 123
   end do

! collecting the best models from all CPU cores
   if(mpirank>0) then
     call mpi_send(totalm,1,mpi_integer8,0,1,mpi_comm_world,status,mpierr)
   else
     do i=1,mpisize-1
       call mpi_recv(bigm,1,mpi_integer8,i,1,mpi_comm_world,status,mpierr)
       totalm=totalm+bigm
     end do
   end if
   call mpi_bcast(totalm,1,mpi_integer8,0,mpi_comm_world,mpierr)

   do j=1,min(int8(n_out),totalm)
      loc=minloc(select_overlap_n)
      do k=1,min(int8(n_out),totalm)
        if(select_overlap_n(k)==select_overlap_n(loc(1)) .and. &
           select_overlap_area(k)<select_overlap_area(loc(1))) loc(1)=k
      end do
      k=loc(1)
      mscore(j,:)=(/dble(select_overlap_n(k)),select_overlap_area(k)/)

      if(mpirank>0) then
        call mpi_send(mscore(j,:),2,mpi_double_precision,0,1,mpi_comm_world,status,mpierr)
      else
        mpicollect(1,:)=mscore(j,:)
        do i=1,mpisize-1
          call mpi_recv(mpicollect(i+1,:),2,mpi_double_precision,i,1,mpi_comm_world,status,mpierr)
        end do
        loc=minloc(mpicollect(:,1))
        do l=1,mpisize
          if(mpicollect(l,1)==mpicollect(loc(1),1) .and. &
             mpicollect(l,2)<mpicollect(loc(1),2) ) loc(1)=l
        end do
        l=loc(1)
      end if
      call mpi_bcast(l,1,mpi_integer,0,mpi_comm_world,mpierr)

      if(mpirank==l-1) then
        mID(j,:ndim)=select_model(k,:ndim)
        select_overlap_n(k)=sum(nsample)*sum(ngroup(:,1000))   ! set to large to avoid to be selected again
      end if
      call mpi_bcast(mID(j,:ndim),ndim,mpi_integer,l-1,mpi_comm_world,mpierr)
      call mpi_bcast(mscore(j,:),2,mpi_double_precision,l-1,mpi_comm_world,mpierr)
   end do

   if(mpirank==0) then
      call writeout2(ndim,mID(1,:ndim),fname,mscore)

2013  format(i20,i20,f20.5,a,*(i8))
      if(n_out>1) then
        write(tname,'(a,i4.4,a,i2.2,a)') 'top',min(int8(n_out),totalm),'_',ndim,'D'
        open(111,file=tname,status='replace')
        write(111,'(4a20)') 'Rank','overlap_n','overlap_area','Feature_ID'
        do i=1,min(int8(n_out),totalm)
           write(111,2013) i,nint(mscore(i,1)),mscore(i,2),'   |',mID(i,:ndim)
        end do
        close(111)
      end if

      if(ndim==2) then
         open(111,file='convex2d_hull',status='replace')
         do itask=1,ntask
         write(111,'(a,i5)') 'Task :',itask
         do i=1,ngroup(itask,1000)
           mm1=sum(ngroup(itask,:i-1))+1
           mm2=sum(ngroup(itask,:i))
           call convex2d_hull(x(mm1:mm2,[mID(1,:ndim)],itask),j,hull)
           write(111,'(a,i5,g20.10)') 'Hull',i,convex2d_area(hull(:j,:))
           do k=1,j
             write(111,'(2e15.5)') hull(k,:)
           end do
         end do
         write(111,'(/)')
         end do
         close(111)
      end if

   end if

end do

call mpi_barrier(mpi_comm_world,mpierr)
end subroutine


subroutine writeout2(ndim,id,fname,mscore)
integer ndim,i,j,id(:),k,l,nh,itask,mm1,mm2,mm3,mm4
real*8 mscore(:,:),tmp,tmp2,hull(maxval(nsample),ndim)
character fname(:)*200,descname*6,tname*3
logical inside

2014 format(a25,*(i8,3a))
2015 format(2a15,*(a,i1.1))
2016 format(2i15,*(e15.5))

descname(:4)='desc'
write(descname(5:5),'(i1.1)') ndim
descname(6:6)='_'
write(1,'(/i2,a)') ndim,'D Descriptor: '
write(1,'(a,i10,a,f15.5)')  'Total data and area in overlapped regions:',int(mscore(1,1)),', ',mscore(1,2)
write(1,2014)   '@@@descriptor: ', (id(i),':[',trim(adjustl(fname(id(i)))),']',i=1,ndim)

do itask=1,ntask
  write(tname,'(i3.3)') itask
  open(10,file='desc_dat/'//descname//tname//'.dat',status='replace')
  write(10,2015) 'Index','classified',('    descriptor_',j,j=1,ndim)
  do i=1,ngroup(itask,1000)  ! the groups in this task $itask
  do j=sum(ngroup(itask,:i-1))+1,sum(ngroup(itask,:i))  ! samples in this group
     inside=.false.
     do k=1,ngroup(itask,1000)  ! between sample j and group k other than i
       if(i==k) cycle
       mm1=sum(ngroup(itask,:k-1))+1
       mm2=sum(ngroup(itask,:k))
       if(ndim==1) then
               if(x(j,id(ndim),itask)>=minval(x(mm1:mm2,id(ndim),itask)) .and. &
                        x(j,id(ndim),itask)<=maxval(x(mm1:mm2,id(ndim),itask)) ) then
                 inside=.true.
                 exit
               end if
       elseif(ndim==2) then
           if(convex2d_in(x(mm1:mm2,[id(:ndim)],itask),x(j,[id(:ndim)],itask),width)) then
             inside=.true.
             exit
           end if
       end if
      end do

    if(inside) then
        write(10,2016) j,0,x(j,[id(:ndim)],itask)  ! unclassified
    else
        write(10,2016) j,1,x(j,[id(:ndim)],itask) ! classified
    end if

  end do
  end do
  close(10)
end do

end subroutine

end program

